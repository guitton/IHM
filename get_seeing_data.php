<?php
include "config.php";  
//include "date_nuit.php";
////////////////////////////////////////////////
function getDataCurveTurb($CurveName)
{
global $db;
global $nb_obs;
global $mediane;
global $jour;
global $lastValue_seeing;
//
global $datedeb;
global $datefin;
 ////////////////////////////
 // dates debut et fin de nuit :
 //$jour="2018-04-26";
 $datedeb="";
 $datefin="";
 getDates(); // ->  $datedeb  et $datefin au format "yyyy-mm-jj hh:mn:ss"
 $sql=  "SELECT * FROM `cats_gdimm_data` WHERE timestamp >= '".$datedeb."' and  timestamp <= '". $datefin."' ORDER BY timestamp ";
 //echo $sql."<br>";
 //////////////////////////////////////////////////////////////////////////////////
 $result = "";
 $req = mysqli_query($db,$sql) or die('Erreur SQL !<br>'.$sql.'<br>'.mysqli_error());
 $cpt = 0;
 $lastValue_seeing = 999;
 while($data = mysqli_fetch_assoc($req)) { 
  $value = $data[$CurveName];
  $n[]= $data[$CurveName];
  $lastValue_seeing = $value;
  $d=$data['timestamp'].' GMT'; // format YYYY-MM-DD HH:MM:SS 
  //echo $data['timestamp']. $d."<br>";
  // Les donnees GDIMM sont en heure TU
  $timestamp = strtotime($d)*1000; 
  //echo $datedeb." ".$data['timestamp']." ".$dateval."<br>";
  // strtotime(datexxx) -> heure TU en sec pour datexxx en heure locale
  if ($cpt == 0) {
   $result = " [".((strtotime($datedeb))*1000).", null],";
   $cpt++;
   $result = $result. " [". $timestamp .",".$value."]";
  } else {
    $result = $result. ", [". $timestamp .",".$value."]";
  }
  $cpt++;
 }
 ////////////////////////////////////////////////////////
 if ($cpt == 0) {
  $result = " [".((strtotime($datedeb))*1000).", null]"; 
  $mediane=null;
 } else {
 // Recherche de la mediane:
 // $n est le tableau de nombres.
     sort($n);
     $c = count($n);
     if ($c % 2){ // cas impair
      $mediane = $n[($c - 1) / 2];
      } else {
      $mediane = ($n[$c/2] + $n[$c/2 - 1]) / 2;
     }
 }
 $result = $result.", [".((strtotime($datefin))*1000).", null]";
 $cpt++; 
 $nb_obs=$cpt;
 return $result;
}


function getFinalCurveTurb($tab_name_r0)
{
 // protect
 global $nb_obs;
 global $lastValue_seeing;
 global $lastValue_seeing_precedente;
 global $graphdome_data;
 global $graphdomestatus_data;
 global $aff_seeing;
 global $mediane;
 global $mediane_epsL;
 global $mediane_epsT;
 //
 $curves_turb = "series:[";
 for($i = 0; $i < count($tab_name_r0); ++$i) {
  $CurveName = $tab_name_r0[$i];
  $result  = getDataCurveTurb($CurveName); 
  $tab_title_r0 = $CurveName;
  if ($CurveName == "epsT") $tab_title_r0 ="Trans";
  if ($CurveName == "epsL") $tab_title_r0 ="Long";
  if ($CurveName == "IndScix100") $tab_title_r0 ="Scintillation (%)";  
  if ($CurveName == "Isop") $tab_title_r0 ="Angle isoplanetique"; 
  $tab_donnes_r0 = $result;
  if  ($CurveName == "epsL") {
   $curves_turb = $curves_turb. sprintf("{ name:'%s',data:[%s] } ]",$tab_title_r0  ,$tab_donnes_r0);
   $mediane_epsL=$mediane;
  } else {
   if  ($CurveName == "epsT") {
    $curves_turb = $curves_turb. sprintf("{ name:'%s',data:[%s] },",$tab_title_r0  ,$tab_donnes_r0);
    $mediane_epsT=$mediane;
   } else {
    $curves_turb = $curves_turb. sprintf("{showInLegend:false, name:'%s',data:[%s]}",$tab_title_r0 ,$tab_donnes_r0);
   }
  }
 }
 if ($CurveName == "IndScix100") {
  $blanc=" ";
  if ( $graphdomestatus_data > $blanc ) { 
   $curves_turb = $curves_turb .' ,'. $graphdome_data  .' ,'. $graphdomestatus_data . " ], labels: {items:[{ html:'meteo authorization',style: { left:'110%',top:'10px',color:'#003366'}},{ html:'dome status',style: { left:'110%',top:'-1px',color:'#339999'}}]}";
  } else {
   if ( $graphdome_data > $blanc ) {      
    $curves_turb = $curves_turb .' ,'. $graphdome_data  . " ], labels: {items:[{ html:'meteo authorization',style:{ left:'110%',top:'10px',color:'#003366'}}]}";
   } else { 
//echo 'empty Scint ';
    $curves_turb = $curves_turb . "]";
   }
  }
  return $curves_turb;
 }
 if  ($CurveName == "epsT") {
  $lastValue_seeing_precedente=$lastValue_seeing;
 }
 if  ($CurveName == "epsL") {
  $seeing=$lastValue_seeing_precedente+$lastValue_seeing;
  if ($nb_obs > 1 )  {
echo $nb_obs;
   if ( $aff_seeing < 0 ) {
    $lastV=intval($seeing*10/2);
    $seeing=$lastV / 10;
    $curves_turb = $curves_turb.", labels: { items:[{html:'LAST SEEING : ". $seeing.
                 "',style: {left:'100%',top:'5px',color:'yellow', fontSize: '15px'}}]}";
   } 
  } else {  
    $curves_turb = $curves_turb." ,
    labels: { items:[{html:'NO OBSERVATIONS ',style: {left:'60%',top:'5px',color:'yellow', fontSize: '15px'} } ] } ";
  }
 
  return $curves_turb;
 } 
 $curves_turb = $curves_turb . "]";
 return $curves_turb;
}

///////////////////////////////////////////////////////////////////////////////////
$host='cats-frontal.oca.eu';
$db = mysqli_connect($host, $login, $pass,'cats');
// Check connection
if ($db) { // get data for HIGHCHARTS 
 $tab_name_eps =  array ("epsT","epsL");
 $graph3_data= getFinalCurveTurb($tab_name_eps);
 //echo "<br>".$graph3_data."<br>";
 $tab_name_r0 =  array ("IndScix100");
 $graph33_data= getFinalCurveTurb($tab_name_r0);
 $mediane_33=$mediane;
// echo $graph33_data."<br>";
 $tab_name_r0 =  array ("Isop");
 $graph333_data= getFinalCurveTurb($tab_name_r0);
 //echo $graph333_data."<br>";
 $mediane_333=$mediane;
 mysqli_close($db); 
}
?>
