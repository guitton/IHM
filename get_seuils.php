<?php
include "config.php";
global $jour;
global $datedeb;
global $datefin;
//
// Connexion et s�lection de la base
$db = mysqli_connect($host, $login, $pass,'cats');
if ( ! $db) {
  $rainrate_max='unknown';
  $windmax='unknown'; 
  $humidity_max='unknown';
  $cloudy_max='unknown';
  $dewpoint_max='unknown';
} else {
 ////////////////////////////
 // dates debut et fin de nuit :
 //$jour="2018-04-26";
 $datedeb="";
 $datefin="";
 getDates(); // ->  $datedeb  et $datefin au format "yyyy-mm-jj hh:mn:ss"
 $sql = "SELECT * FROM `cats_weather_pref`" ;
 $result = "";
 $req = mysqli_query($db,$sql) or die('Erreur SQL !<br>'.$sql.'<br>'.mysqli_error());
 while($data = mysqli_fetch_assoc($req)) { 
  $rainrate_max=$data['rainrate_max'];
  $windmax=$data['wind_max']/3.6; //conversion km/h en m/s
  $humidity_max=$data['humidity_max'];
  $cloudy_max=$data['cloudy_max'];
  $dewpoint_max=$data['dewpoint_max'];
 }
 mysqli_close($db); 
 $result = " [". (strtotime($datedeb)*1000) .",".$windmax."],";
 $result = $result. " [". (strtotime($datedeb)*1000+1000) .",".$windmax."],";
 $result = $result. " [". (strtotime($datefin)*1000-1000) .",".$windmax."],";
 $result = $result. " [". (strtotime($datefin)*1000) .",".$windmax."]";
 $graph_windmax = sprintf("{type:'line', color:'#55EEEE', lineWidth:'0.7', showInLegend:false, name:'' ,data:[%s]}",$result);

 $result = " [". (strtotime($datedeb)*1000) .",".$humidity_max."],";
 $result = $result. " [". (strtotime($datedeb)*1000+1000) .",".$humidity_max."],";
 $result = $result. " [". (strtotime($datefin)*1000-1000) .",".$humidity_max."],";
 $result = $result. " [". (strtotime($datefin)*1000) .",".$humidity_max."]";
 $graph_humiditymax = sprintf("{type:'line', color:'#55EEEE', lineWidth:'0.7', showInLegend:false, name:'' ,data:[%s]}",$result);

 $result = " [". (strtotime($datedeb)*1000) .",".$cloudy_max."],";
 $result = $result. " [". (strtotime($datedeb)*1000+1000) .",".$cloudy_max."],";
 $result = $result. " [". (strtotime($datefin)*1000-1000) .",".$cloudy_max."],";
 $result = $result. " [". (strtotime($datefin)*1000) .",".$cloudy_max."]";
 $graph_cloudymax = sprintf("{type:'line', color:'#55EEEE', lineWidth:'0.7', showInLegend:false, name:'' ,data:[%s]}",$result);
}
//echo $graph_cloudymax;
?>
