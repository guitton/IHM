<?php
include "config.php";
$last_mesure_meteo = 300;
$lastValue_meteo = 0;
$lastDateAcq_meteo = "";
#error_reporting(E_ALL);
error_reporting(E_ALL ^ E_DEPRECATED);
//////////////////////////////
//////////////////////////////////////////////////
function getDataCurvemeteo($CurveName)
{
 global $last_mesure_meteo ;
 global $lastValue_meteo ;
 global $lastDateAcq_meteo ;
 global $graph_windmax;
 global $graph_humiditymax;
 global $db;
 global $jour;
 global $yy;
 global $mm;
 global $jj;
 include "ephemerides.php"; // tables lever/coucher du soleil
  
 // heure TU
 $now_hour=date('G');
 //echo $now_hour ."<br>";
 // Calcul des offsets TU / heure locale
 $d=new DateTime("now");
 $tz=new DateTimeZone('Europe/Paris');
 $d->setTimezone($tz);
 $offset=$d->getOffset();
 //echo $offset."<br>";
 $offsethour=$offset/3600;
 //echo $offsethour ."<br>";
 // Les donnees meteo sont en heures TU: on les affiche en heure locale
 //////////////////////////////////////
 // On cherche les heures de lever et coucher du soleil (h locale) qui 
 // dependent du numero de jour dans l'annee:
 $numjour=date('z');
 $heurelever=$hlever[$numjour]+1; //1H avant
 $heurecoucher=$hcoucher[$numjour];//1H apres
 // On passe en heure TU
 $heurecouchertu=$heurecoucher-$offsethour; 
 $heurelevertu=$heurelever-$offsethour;
 // On observe entre $heurelever et $heurecoucher heure locale
 // les donnees meteo sont datees en heure TU
 if ($now_hour < $heurecoucher) { // acq terminee pour le jour en cours ou 2nde partie de la nuit
   //echo " acq terminee pour le jour en cours ou 2nde partie de la nuit"."<br>";
   //
   $dd = date('Y-m-d',time()-3600*24*1);
   $datedeb=$dd.' '.$heurecoucher.':00:00';
   $datedebtu=$dd.' '.$heurecouchertu.':00:00';
   //
   $dd = date('Y-m-d',time());
   $datefin = $dd.' '.$heurelever.':00:00';
   $datefintu = $dd.' '.$heurelevertu.':00:00';
   //
   $yeardeb = intval(date('Y',time()-3600*24));
   $moisdeb = intval(date('m',time()-3600*24))-1;
   $jourdeb = intval(date('d',time()-3600*24));
   $yd = intval(date("Y",time()-3600*24)); 
   $md = intval(date("m",time()-3600*24))-1;
   $dd = intval(date("d",time()-3600*24));
   $yf = intval(date("Y",time())); 
   $mf = intval(date("m",time()))-1;
   $df = intval(date("d",time()));
  } 
  else { // affiche 1ere partie de la nuit
    //echo "1ere partie de la nuit "."<br>";    
    //
    $dd = date('Y-m-d',time());
    $datedeb=$dd.' '.$heurecoucher.':00:00';
    $datedebtu=$dd.' '.$heurecouchertu.':00:00';
    //
    $dd = date('Y-m-d',time()+3600*24*1);
    $datefin = $dd.' '.$heurelever.':00:00';
    $datefintu = $dd.' '.$heurelevertu.':00:00';
    //
    $yeardeb = intval(date('Y'));
    $moisdeb = intval(date('m'))-1;
    $jourdeb = intval(date('d')); 
    $yd = intval(date("Y",time())); 
    $md = intval(date("m",time()))-1;
    $dd = intval(date("d",time()));
    $yf = intval(date("Y",time()+3600*24)); 
    $mf = intval(date("m",time()+3600*24))-1;
    $df = intval(date("d",time()+3600*24)); 
  }


 $sql = "SELECT ". $CurveName.",dateTime FROM `archive` where dateTime >= '". strtotime($datedeb)."' and dateTime <= '".strtotime($datefin)."'" ;
 $result = "";
 $cpt = 0;
 $value="null";
 $nbpmax=20;
 $nbpcourant=19;
 //echo "<br>";
 echo $sql."<br>     ".$datedeb."    ->    ".$datefin."<br>";
 if ($db) {
 $req = mysqli_query($db,$sql) or die('Erreur SQL !<br>'.$sql.'<br>'.mysqli_error());
 echo $cpt.' '.$nbpcourant."<br>";
 while  ($data = mysqli_fetch_assoc($req)) {
  $nbpcourant++;
  $cpt++;
  if ( $nbpcourant >= $nbpmax ) {
   $nbpcourant=1; 
   $lastvalue=$value;
   $value = $data[$CurveName];
   $lastValue_meteo = $value;
   if ( $value == "" ) {
    $value=$lastvalue;
   } else {
    if ( $CurveName == "windSpeed" )  $value=$value/3.6;
   }
   $timestamp = $data['dateTime']*1000; // date TU 
   $y_t=$data['dateTime'];  
   $year = intval(date("Y", $y_t));
   $month = intval(date("m", $y_t))-1;
   $day = intval(date("d", $y_t));
   $hour = intval(date("H", $y_t));
   $minute = intval(date("i", $y_t));
   $second = intval(date("s", $y_t));
   $datetime = sprintf("%d,%d,%d,%d,%d,%d",$year,$month,$day,$hour,$minute,$second);
   echo $datetime."<br> ";
   if ($cpt == 1) {
    $value0= "null";
$datetime0="1970-01-01 00:00:00 GMT"; 
 echo ' deb: '.$datetime0." ".(strtotime($datetime0))*1000 ."<br> ";
    $datetime0= sprintf("%d-%.02d-%.02d %.02d:00:00 GMT",$yd,$md,$dd,$heurecoucher+$offsethour);
    echo ' deb: '.$datetime0."<br> ";
    $result = $result." [".((strtotime($datetime0))*1000).",".$value0."],";
   echo $datetime."<br> ";
    $result = $result. " [".$timestamp.",".$value."]";
    //$lastValue_meteo = $value;
    $lastDateAcq_meteo = $hour."h".$minute."\' ";
   } else {
   echo $datetime."<br> ";
    $result = $result. ", [".$timestamp.",".$value."]";
   }
  }
 }
}
 if ($cpt  > 0) {
   $value0= "null";
   $datetime0= sprintf("%d-%.02d-%.02d %.02d:00:00 GMT",$yf,$mf,$df,$heurelever);
echo ' fin:'.$datetime0."<br> ";
   $result = $result." ,[".((strtotime($datetime0))*1000).",".$value0."]";
 }
 if ($cpt == 0) {
  $result = "";
  $value="null";
  $datetime0= sprintf("%d-%.02d-%.02d %.02d:00:00 GMT",$yd,$md+1,$dd,$heurecoucher);
  $result = $result." [".((strtotime($datetime0))*1000)."," .$value."],"; 
  $datetime0= sprintf("%d-%.02d-%.02d %.02d:00:00 GMT",$yf,$mf+1,$df,$heurelever);
  $result = $result." [".((strtotime($datetime0))*1000).",".$value."]";
 }
 return $result;
}

function getFinalCurvemeteo($tab_name_meteo)
{
 // protect
 global $lastValue_meteo;
 global $lastValue_meteo_precedente;
 global $lastDateAcq_meteo;
 global $graph_windmax;
 global $graph_humiditymax;
 global $dewpoint_max;
 global $humidity_max;
 global $windmax;
 global $db;
 unset($tab_title_meteo);
 unset($tab_donnes_meteo);
 unset($tab_lastValue_meteo);
 for($i = 0; $i < count($tab_name_meteo); ++$i) {
  $CurveName = $tab_name_meteo[$i];
  $lastValue_meteo_precedente=$lastValue_meteo;
  $result  = getDataCurvemeteo($CurveName);
  $tab_title_meteo[] = $CurveName;
  $tab_donnes_meteo[] = $result;
  $tab_lastValue_meteo[] = sprintf("%0.1f",$lastValue_meteo);
 }
 $curves_meteo = "series:[";
 for($i = 0; $i < count($tab_title_meteo); ++$i) {
  $last_value_meteo = $tab_lastValue_meteo[$i];
  $CurveName = $tab_name_meteo[$i];
  if ($CurveName == "windSpeed" ) {
       $tab_title_meteo[$i]="Wind";
    $last_value_meteo = $last_value_meteo ."m/s";
    }
  if ($CurveName == "outHumidity" )  {
       $tab_title_meteo[$i]="Humidity ";
    $last_value_meteo = $last_value_meteo ." %";
   } 
   if ($CurveName == "Dewpoint" )  {
       $tab_title_meteo[$i]="Dewpoint";
    $last_value_meteo = $last_value_meteo ." °";
   }
  if ($CurveName == "outTemp" )  {
       $tab_title_meteo[$i]="Temperature";
    $last_value_meteo = $last_value_meteo ." (°C)";
   }
  
   if ($i == (count($tab_name_meteo)-1) ) {
     if ($CurveName == "Dewpoint") {
      $curves_meteo = $curves_meteo. sprintf("{name:'%s',data:[%s]}",$tab_title_meteo[$i]  ,$tab_donnes_meteo[$i]);
     }
     else {
      $curves_meteo = $curves_meteo. sprintf("{showInLegend:false, name:'%s',data:[%s]},",$tab_title_meteo[$i]  ,$tab_donnes_meteo[$i]);
     }
   }
   else {
    $curves_meteo = $curves_meteo. sprintf("{name:'%s',data:[%s]},",$tab_title_meteo[$i] ,$tab_donnes_meteo[$i]);
   }
 }
/////////////////////////////////
 if ($CurveName == "windSpeed"){ 
   $lv_d =intval($windmax*10) ; 
   $lastV = $lv_d / 10 ;
   $lastValue_meteo=$lastValue_meteo/3.6;
   $lv_dc =intval($lastValue_meteo*10) ; 
   $lastVc = $lv_dc / 10 ;
   if ( $lastValue_meteo > $windmax ) {
     $curves_meteo = $curves_meteo .' '. $graph_windmax . "], labels: {
		 items:[{html:'Wind speed ". $lastVc . "m/s > ". $lastV . "m/s',style: {left:'55%',top:'1px',color:'#990000'}}]}";
   }
   else {
     $curves_meteo = $curves_meteo .' '. $graph_windmax . "], labels: {
		 items:[{html:'Wind speed threshold = ". $lastV . "m/s',style: {left:'55%',top:'1px',color:'#55EEEE'}}]}";
   }
 }
 else if ($CurveName == "outHumidity") { 
   $lv_dc =intval($lastValue_meteo) ; 
   $lastVc = $lv_dc ;
   if (!$db) { 
      $curves_meteo = $curves_meteo .' '. $graph_humiditymax . "], labels: {
	 items:[{html:'No access to METEO host',style:{left:'50%',top:'60px',color:'yellow'}},{html:'Humidity threshold= ". $humidity_max . "%',style:{left:'50%',top:'2px',color:'#55EEEE'}}]}";
   } else {
   if ( $lastValue_meteo > $humidity_max ) {
    $curves_meteo = $curves_meteo .' '. $graph_humiditymax . "], labels: {
		 items:[{html:'Humidity threshold: ". $lastVc."% > " . $humidity_max . "%',style: {left:'50%',top:'2px',color:'#990000'}}]}";
   }
   else {
     $curves_meteo = $curves_meteo = $curves_meteo .' '. $graph_humiditymax . "], labels: {
	 items:[{html:'Humidity threshold= ". $humidity_max . "%',style:{left:'50%',top:'2px',color:'#55EEEE'}}]}";
    
   }
  }
  }
 else if ($CurveName == "Dewpoint") { 
   $diff=($lastValue_meteo_precedente-$lastValue_meteo);
   $lv_inter=intval($lastValue_meteo_precedente*10);
   $lv_p=$lv_inter/10; // temperature
   $lv_inter=intval($lastValue_meteo*10);
   $lv_d=$lv_inter/10; // dewpoint
   $lastV=intval($diff*10);
   $diff=$lastV / 10;
   if (!$db) { $diff=$dewpoint_max;}
   if ( $diff < $dewpoint_max ){
     $curves_meteo = $curves_meteo . "], labels: { items:[{html:'Dewpoint depression threshold : ". $diff."°C( ". $lv_p."- ".$lv_d." <" . $dewpoint_max . ")',style: {left:'0%',top:'5px',color:'#990000'}}]}";
   }
   else {
     $curves_meteo = $curves_meteo . "], labels: { items:[{html:'Dewpoint depression threshold = ". $dewpoint_max . 
   "°C ',style: {left:'30%',top:'5px',color:'#55EEEE'}}]}";
   }
  }
 else {
  $curves_meteo = $curves_meteo . "]";
 }
 

 return $curves_meteo;
}

// DATA FOR GRAPHIC
// Connexion et sélection de la base meteo
$db = mysqli_connect($host_meteo, $login_meteo, $pass_meteo,'weewx');
//////////////////////////
$tab_name_meteo =  array ("outHumidity");
$graph22_data= getFinalCurvemeteo($tab_name_meteo);
echo '<br>';
echo $graph22_data;
$tab_name_meteo =  array ("windSpeed");
$graph222_data= getFinalCurvemeteo($tab_name_meteo);

$tab_name_meteo =  array ("outTemp","Dewpoint");
$graph2222_data= getFinalCurvemeteo($tab_name_meteo);
//echo $graph2222_data;

//////////////////////////
if ($db) {
 mysqli_close($db);
}
?>
