<?php
include "config.php";
#error_reporting(E_ALL);
error_reporting(E_ALL ^ E_DEPRECATED);
//////////////////////////////
//////////////////////////////////////////////////
function getData()
{global $db;
 include "ephemerides.php"; // tables lever/coucher du soleil
 $tabresN=0;
 $tabresNE=0;
 $tabresE=0;
 $tabresSE=0;
 $tabresS=0;
 $tabresSW=0;
 $tabresW=0;
 $tabresNW=0;
 // heure TU
 $now_hour=date('G');
 //echo $now_hour ."<br>";
 // Calcul des offsets TU / heure locale
 $d=new DateTime("now");
 $tz=new DateTimeZone('Europe/Paris');
 $d->setTimezone($tz);
 $offset=$d->getOffset();
 //echo $offset."<br>";
 $offsethour=$offset/3600;
 //echo $offsethour ."<br>";
 // Les donnees meteo sont en heures TU: on les affiche en heure locale
 // mais datation systeme en heures TU
 //////////////////////////////////////
 // On cherche les heures de lever et coucher du soleil (h locale) qui 
 // dependent du numero de jour dans l'annee:
 $numjour=date('z');
 //echo $numjour."<br>";
 //echo "hl=".$hlever[$numjour]."<br>";
 //echo "hc=".$hcoucher[$numjour]."<br>";
 $heurelever=$hlever[$numjour]+1; //1H avant
 $heurecoucher=$hcoucher[$numjour];//1H apres
 // On passe en heure TU
 $heurecouchertu=$heurecoucher-$offsethour; 
 $heurelevertu=$heurelever+1-$offsethour;
 // On observe entre $heurelever et $heurecoucher heure locale
 // les donnees meteo sont datees en heure TU
     if ($now_hour < $heurecoucher) { // acq terminee pour le jour en cours ou 2nde partie de la nuit
        //echo " acq terminee pour le jour en cours ou 2nde partie de la nuit"."<br>";
  //
  $dd = date('Y-m-d',time()-3600*24*1);
        $datedeb=$dd.' '.$heurecoucher.':00:00';
        $datedebtu=$dd.' '.$heurecouchertu.':00:00';
  //
  $dd = date('Y-m-d',time());
  $datefin = $dd.' '.$heurelever.':00:00';
  $datefintu = $dd.' '.$heurelevertu.':00:00';
  //
  $yeardeb = intval(date('Y',time()-3600*24));
  $moisdeb = intval(date('m',time()-3600*24))-1;
  $jourdeb = intval(date('d',time()-3600*24));
  $yd = intval(date("Y",time()-3600*24)); 
     $md = intval(date("m",time()-3600*24))-1;
     $dd = intval(date("d",time()-3600*24));
  $yf = intval(date("Y",time())); 
     $mf = intval(date("m",time()))-1;
     $df = intval(date("d",time()));
  } 
  else { // affiche 1ere partie de la nuit
    //echo "1ere partie de la nuit "."<br>";    
    //
    $dd = date('Y-m-d',time());
          $datedeb=$dd.' '.$heurecoucher.':00:00';
          $datedebtu=$dd.' '.$heurecouchertu.':00:00';
    //
    $dd = date('Y-m-d',time()+3600*24*1);
       $datefin = $dd.' '.$heurelever.':00:00';
       $datefintu = $dd.' '.$heurelevertu.':00:00';
    //
    $yeardeb = intval(date('Y'));
    $moisdeb = intval(date('m'))-1;
    $jourdeb = intval(date('d')); 
    $yd = intval(date("Y",time())); 
     $md = intval(date("m",time()))-1;
       $dd = intval(date("d",time()));
    $yf = intval(date("Y",time()+3600*24)); 
       $mf = intval(date("m",time()+3600*24))-1;
       $df = intval(date("d",time()+3600*24)); 
 }
 $sql = "SELECT windDir FROM `archive` where dateTime > '". strtotime($datedebtu) . "' and dateTime < '". strtotime($datefintu) . "'"  ;
 $result = "";
 $cpt = 0;
 if ($db) {
 //echo "<br>";
 //echo $sql."     ".$datedeb." -> ".$datefin."<br>";
 $req = mysqli_query($db,$sql) or die('Erreur SQL !<br>'.$sql.'<br>'.mysqli_error());
 $pas=45/2;
 while($data = mysqli_fetch_assoc($req))
 { 
  $value = $data['windDir'];
  if ($value < 0) {
  $value=$value+360; }
  if (($value < $pas) && ($value > (360-$pas))){
   $tabresN =  $tabresN +1;
  } elseif (($value < 45+$pas) && ($value > (45-$pas))){
     $tabresNE =  $tabresNE +1;
  } elseif (($value<90+$pas) && ($value>(90-$pas))){
    $tabresE =  $tabresE +1;
  } elseif (($value<135+$pas) && ($value>(135-$pas))){
    $tabresSE =  $tabresSE +1;
  } elseif (($value<180+$pas) && ($value>(180-$pas))){
    $tabresS =  $tabresS +1;
  } elseif (($value<225+$pas) && ($value>(225-$pas))){
    $tabresSW = $tabresSW +1;
  } elseif (($value<270+$pas) && ($value>(270-$pas))){
    $tabresW = $tabresW +1;
  } else {
    $tabresNW = $tabresNW +1;
  }
 
  $cpt++;
 }
 }
 if ($cpt  > 0) {
 $tabresN = $tabresN * 100 / $cpt;
 $tabresNE= $tabresNE * 100 / $cpt;
 $tabresE= $tabresE * 100 / $cpt;
 $tabresSE= $tabresSE * 100 / $cpt;
 $tabresS= $tabresS * 100 / $cpt;
 $tabresSW= $tabresSW * 100 / $cpt;
 $tabresW= $tabresW * 100 / $cpt;
 $tabresNW= $tabresNW * 100 / $cpt;
 $result=sprintf('data:[[0,%.2f],[45,%.2f],[90,%.2f],[135,%.2f],[180,%.2f],[225,%.2f],[270,%.2f],[315,%.2f]]',
  $tabresN,$tabresNE,$tabresE,$tabresSE,$tabresS,$tabresSW,$tabresW,$tabresNW);

 }

    return $result;
}

// DATA FOR GRAPHIC
// Connexion et s�lection de la base meteo
$db = mysqli_connect($host_meteo, $login_meteo, $pass_meteo,'weewx');
//////////////////////////
$windir_data= getData();
//echo $windir_data;

//////////////////////////
if ($db) {
mysqli_close($db);
}
?>
