<?php
/**
* @class cats_Allsky
* @details connect by tcp/ip on AllSky software and display
* status in webpage
* Note: You must have extension=php_sockets.dll actived in php.ini (version wamp)
* usage:
  exemple for index.php
	include "status_allsky.php";
	$allsky = new cats_allSky("192.134.16.150",5555,true);
	ou
	$allsky = new cats_allSky($host_tcp,$port_tcp,true);
	les variables $host_tcp, $port_tcp ainsi que les parametres de connexion mysql sont
	dans le fichier  config.php
	
	si true = connexion tcp/ip
	si false = connexion mysql
	
	Tips: For refresh page avery 10 s add <meta http-equiv="refresh" content="10" > 
*
*
*/
include "config.php"; //"gdimm_config.php";
# permet d'ignorer le message de mysql_connect is deprecated la nouvelle biblio est mysqli
error_reporting(E_ALL ^ E_DEPRECATED);
class gdimm_status
{
	private $host_tcp = "192.134.16.150";  //192.134.16.150
	private $port_tcp = "5555";
	private $status = "CRITICAL";

	// Connexion et s�lection de la base

    function __construct($host,$port,$is_socket) {
	
		global $host_tcp;
		global $port_tcp;
		$this->host    = $host_tcp;
		$this->port    = $port_tcp;
		$this->status = "CRITICAL";
		global $mode;
		global $date;
		global $heure;
		$mode = $is_socket;
		$sequence = "stopped";
		
		// test si fonctionnement en socket ou sur la database
		if ($mode)
		{
			$date = date("d-m-Y");
			$heure = date("H:i");			
			$status = $this->send_msg("get_status");
		} else
		{
			$reply = $this->get_status_from_db();
			$date= $reply[0];
			$heure= $reply[1];
			$sequence = $reply[2];
			$status = $reply[3];
			$cloudy = $reply[4];
		}

		$this->Display_in_tab($date,$heure,$sequence,$status,$cloudy);	
    }
	


			/**
	* @brief send message to allsky software
	* @return reply message
	*/
	function disp_error($message)
	{
			// display button
		$date = date("d-m-Y");
		$heure = date("H:i");
		echo" <table border='0' cellspacing='5' BGCOLOR='#0000FF'>
		   <!-- Ici, on �crira le contenu du tableau -->
		   <tr ALIGN=CENTER>
				<td colspan='3'> <FONT COLOR='white'><b>CATS GDIMM</b> </td>
		   </tr>
		   <tr> 
				
				<td bordercolor='#000000' bgcolor='#FF0000'>".$date." at ".$heure." <i></i></td>
				<td bordercolor='#000000' bgcolor='#FF0000'> NA</td>
				<td bordercolor='#000000' bgcolor='#FF0000'> ". $message . "</td>
		   </tr>
		</table>";
	}

	/**
	* @brief read status from database
	* @return reply message
	*/
	function get_status_from_db()
	{
		$status = 0;
		$date = "21-10-2015";
		$heure = "14:36";
		$message = "NA";
		$value = "0";
		global $host;
		global $login;
		global $pass;
		// get status from DB
		// Connexion et s�lection de la base
		$message="ERROR : Can't connect to database";
		$date = date("d/m/Y");
		$heure = date("H:i:s");
		$value="ERROR";
		$sequence="Unknown status";
		$db1 = mysqli_connect($host, $login, $pass,'cats');
                $sql = "SELECT * FROM `cats_status` WHERE `id_instrument` = 1" ;
		$req = mysqli_query($db1,$sql);
		while($data = mysqli_fetch_assoc($req))
		 { 
			$MysqlTimestamp = strtotime($data['timestamp']);
			$message = $data['message'];
			$value = $data['value'];
			$status = $data['status_instr'];
			$sequence = $data['sequence'];
			if ($sequence == 0) {
			 $sequence = "Stopped";
			}
			else {
			  $sequence = "Running";
			}
			// $status = 4    --> mode LIVE
			if ($status == 0) {
				$status = "SUCCESS";
			} 
			else {
				$status = "CRITICAL";
			}
			$date = date("d/m/Y", $MysqlTimestamp);
			$heure = date("H:i:s", $MysqlTimestamp);
		 }
		 $localTimestamp = time() - date('Z');
		 // marge d'erreur entre les horloges
		 if ( $localTimestamp  > ($MysqlTimestamp+5) || $localTimestamp  < ($MysqlTimestamp-5) ) 
		 {
			//$this->disp_error("!!GDIMM is not started");
			$message="GDIMM is not started !";
			$value="ERROR";
		 }

		$reponse = array($date,$heure,$sequence,$message,$value);
		return $reponse;
	}
	
	/**
	* @brief send message to allsky software
	* @param[in] message ie. get_status
	* @return reply message
	*/
	function send_msg($message)
	{
		// create socket
		$socket = socket_create(AF_INET, SOCK_STREAM, 0) ;
		if (!$socket)
		{
			$this->disp_error("ERROR :Can't create socket.");
			
		}
		// connect to server
		echo '<!--';
		$result = socket_connect($socket, $this->host, $this->port); 
		echo '-->';
		if (!$result)
		{
			$this->disp_error("ERROR : Could not connect to GDIMM");
			
		}
		// send string to server
		$result = socket_write($socket,$message, strlen($message));
		if (!$result)
		{
			$this->disp_error("ERROR : Could not send data to GDIMM");
			
		}
		// get server response
		$result = socket_read ($socket, 2048,PHP_NORMAL_READ);
		if (!$result)
		{
			$this->disp_error("ERROR : Could not read server response from GDIMM");
			
		}
		// close socket
		socket_close($socket);
		return $result;
	}

	/**
	* @brief display information
	* @details display information in table
	*/
	function Display_in_tab($date,$heure,$sequence,$status,$message)
	{ 
		// display button
		global $mode;
		$title = "GDIMM ";
		$bleu="'#00FFFF'";
		$vert="'#00FF00'";
		$jaune="'#FFFF00'";
		$rouge="'red'";
		$statussequence= $vert;
		$statuscolor= $vert;
		$statusdate=$bleu;
		if ( substr($sequence,0,3) == substr("",0,3 ) )$statussequence= $rouge;
		if ( substr($sequence,0,5) == substr("Unknown",0,5 ) ) {
		 $statussequence=$rouge;
		 $statuscolor=$rouge;
		}
		if ( substr($sequence,0,3) == substr("Run",0,3 )) {
		  if ( substr($status,0,17) == substr("GDIMM is not started",0,17 )) {
		    $statusdate= $rouge;
			$statussequence= $rouge;
			$statuscolor= $rouge;}
			}
		if ( substr($sequence,0,3) == substr("ONLINE",0,3 ) )$statussequence= $vert;
		if ( substr($sequence,0,3) == substr("Sto",0,3 ) )$statussequence= $jaune;
		
		if ( substr($status,0,3) == substr(" OFFLINE",0,3 ) )$statuscolor= $rouge;
		if ( substr($status,0,3) == substr("ONLINE",0,3 ) )$statuscolor= $vert;
		if ( substr($status,0,3) == substr("PAUSE",0,3 ) )$statuscolor= $jaune;
		if ($mode)
		{
			$title = "CATS GDIM";
		}

		echo" <table border='0' cellspacing='2' BGCOLOR='#0000FF'>
		   <!-- Ici, on �crira le contenu du tableau -->
		   <tr ALIGN=CENTER>
				<td colspan='3'> <FONT COLOR='white'><b>" . $title . "</b> </td>
		   </tr>
		   <tr> 
				
				<td bordercolor='#000000' bgcolor=". $statusdate ." ><font size='2'>".$date." at ".$heure." <i>UT</font></i></td>
				<td bordercolor='#000000' bgcolor=". $statussequence ."><font size='2'>"
				.$sequence.
				 "</font></td>".
				"<td bordercolor='#000000' bgcolor=".$statuscolor."  ><font size='2'> " . $status . " </font></td>
		   </tr>
		</table>";
	}
	

}

?>
