#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <string.h> //strlen
#include <sys/socket.h>
#include <arpa/inet.h> //inet_addr
#include <unistd.h> //write
#include <stdbool.h>
#include <pthread.h>
////////////////////////
///////////////////////////////////////////
//
// compiler par : 
//  g++ -I ./  -c -o getparam.o getparam.cpp
//  gcc -Wall serveur_automate.c getparam.o -o serveur_automate -fno-builtin -lpthread
///////////////////////////////////////////
#define PORT 10001   // The first 1000 ports are reserved for specific applications on linux
#define NUM_THREADS 2
/////////////////////////////////////////// 
extern string toit;
extern automate;
extern secteur;
extern alim;
// lecture des parametres :
extern int getparam();
///////////////////////
// Variables globales:
time_t t;
struct tm tm;
int client_sock, read_size;
bool mess1, mess2;
bool breakreadwrite;
FILE *trace;
int status;

///////////////////////////////////////////////////////
void *readsocket(){
 //
 int n1;
 char buffer[256];
 char buffertrace[256];
 while (1) {
  bzero(buffer, 256); 
  read_size = recv(client_sock, buffer, 256, 0);
  if (read_size > 0 ){
   t = time(NULL);
   tm = *localtime(&t);
   hms_fincor = tm.tm_hour*60+tm.tm_min;
   printf("Message recu = %s (hh=%d mn=%d %d sec)\n", 
	   buffer, tm.tm_hour, tm.tm_min, tm.tm_sec)	;
   fflush(stdout);
   getparam(); // lecture des etats
   /* messages attendus:  
        $toit!
        $automate!
        $secteur! 
        $alim!
     */ 
    n1=strncmp(buffer, "$toit!", 6); 
    if (n1 == 0) {     
     fflush(stdout);
     bzero(buffer, 256);
     sprintf(buffer, "$toit:%s!", toit);  
     status = write(client_sock, buffer, strlen(buffer));	           
    } else {
     n1=strncmp(buffer, "$automate!", 10); 
     if (n1 == 0) {     
      fflush(stdout);
      bzero(buffer, 256);
      sprintf(buffer, "$automate:%s!",automate );
      status = write(client_sock, buffer, strlen(buffer));	           
     } else {
      n1=strncmp(buffer, "$secteur!", 9); 
      if (n1 == 0) {     
       fflush(stdout);
       bzero(buffer, 256);
       sprintf(buffer, "$secteur:%s!", secteur);
       status = write(client_sock, buffer, strlen(buffer));	           
      } else {
        n1=strncmp(buffer, "$alim!", 6); 
        if (n1 == 0) {     
         fflush(stdout);
         bzero(buffer, 256);
         sprintf(buffer, "$alim:%s!", alim);	 
         status = write(client_sock, buffer, strlen(buffer));	            
        } else {			    
	   printf("message non reconnu %s\n", buffer);
	   fflush(stdout);
           bzero(buffer, 256);
           sprintf(buffer, "$notcommand!");
           status = write(client_sock, buffer, strlen(buffer));	  
         }
        }
       }
      }
  
   } else {
    breakreadwrite=true;
    printf("Connection arretee de la part de l'automate' %d\n", read_size);
    fflush(stdout);
    close(client_sock);
    pthread_exit(NULL);
  }
 } // while (1)	
} // fin de readsocket
////////////////////////////////////////////
//
////////////////////////////////////////////
////////////////////////////////////////////
int main(int argc, char *argv[]) {
 int socket_desc, c;
 struct sockaddr_in server, client;
 char buffer[256];
 size_t buffer_size;
 char * word;
 char * pch1;
 char * pch2 ;
 int status;
 bool fichtrace, findujour;
 //////////////////////////////
 pthread_t threads[1];
 //////////////////////////////////
 t = time(NULL);
 tm = *localtime(&t);
 printf("Debut de serveur automate a %dh%dmn%ds (%d/%d/%d) \n", 
	 tm.tm_hour, tm.tm_min, tm.tm_sec, tm.tm_mday, tm.tm_mon+1, tm.tm_year+1900);		
 fflush(stdout);
 status=0; 
 t = time(NULL);
 tm = *localtime(&t);
 hh=tm.tm_hour;
 mn=tm.tm_min;
 fflush(stdout);
 /////////////////////////////////////////////
 //Create socket
 socket_desc = socket(AF_INET, SOCK_STREAM, 0);
 if (socket_desc == -1) {
  perror(strerror(errno));
  return(-1);
 }
 printf("Socket cree\n");
 fflush(stdout);
 tm = *localtime(&t);
 //Preparation structure sockaddr_in
 server.sin_family = AF_INET;
 server.sin_addr.s_addr = htonl(INADDR_ANY);// INADDR_ANY;
 server.sin_port = htons(PORT );
 if( bind(socket_desc, (struct sockaddr *)&server, sizeof(server)) < 0){
  //print the error message
  perror(strerror(errno));
  fflush(stdout); 
  return(-1);
 }
 /////////////////////////////////////////////////////////
 while ( 0 == 0 ) { 
  // On attend la connection du client :
  /////////////////////////////////////////////////////////
  breakreadwrite=false;
  listen(socket_desc, 3);
  printf("Waiting for incoming connections...\n");
  fflush(stdout);
  c = sizeof(struct sockaddr_in);
  client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);
  if (client_sock < 0){
   perror(strerror(errno));
   return 1;
  }
  t = time(NULL);
  tm = *localtime(&t);
  hh = tm.tm_hour;
  mn = tm.tm_min;
  printf("Client connected %dh %dmn (%d/%d/%d) \n", hh, mn, 
	 tm.tm_mday, tm.tm_mon+1, tm.tm_year+1900);
  fflush(stdout);
  sprintf(buffer, "$CONNECTIONOK!");
  status=write(client_sock, buffer, strlen(buffer)); 
  bzero(buffer, 256); 
  read_size = recv(client_sock, buffer, 256, 0);
  if (read_size > 0 ){
   printf("Message recu = %s \n", buffer)	;
   fflush(stdout);
  }
  /////////////////////////////////////////////////////////
  // Le client est connecte, on gere les messages de correction dans la boucle qui suit
  // lancement du thread de lecture sur le socket
  pthread_create(&threads[0], NULL, readsocket, NULL);
  while (( 1 != 0 ) && (breakreadwrite == false)) {
    /////////////////////////////////////////////////////////
    //
    if (status < 0) {
     breakreadwrite=true;
    }
   } // fin de while ( 1 != 0 ) && (breakreadwrite == false)	  
   sleep(10);
  } // fin de while ( 0 == 0 ) 
 return(0);
}
