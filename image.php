<?php


//
//find date-obs in fits header
//
function getHeader ($image) {
 system('/home/renaud/bin/listheader '.$image.' > lines.txt',$return);
 if ($return==0){

 $test=file('lines.txt');
 $length=count($test);
 $i=0;
 while ( $i<$length)
     {
     $findDate = strpos( $test[$i],'ATE-OBS=');
     if ($findDate == false){
       $i=$i+1;
     }
     else{
	 $line=$test[$i];
	 break;
   }
}	
   $keywords = preg_split("/[\s,]+/", $line); 	
   $date=$keywords[1];
   if ($date<>''){
   return $date;
   }
   else{
   echo '<p> no date available for '.$image.'</p>';
   return false;
   }
}
else{
 echo '<p> error </p>';
return false;
}

}

function getFiles($path){

 //
 //find images
 //
 system('ls -tr '.$path.' > files.txt',$return); 
 if ($return==0){
   $test=file('files.txt', FILE_IGNORE_NEW_LINES);
   $length=count($test);
   $j=$length-4;
   $i=$length-1;
   $k=0;
   $name=array(3); 

   while ($i>$j){
      //
      //get date
      //
      $date=getHeader($path.$test[$i]); 
   

     //
     //convert fits -> png
     //
     $aux=explode('.',$test[$i]);    
     $commandLine='convert -verbose -normalize -geometry 512x512 '.$path.$test[$i].' '.$aux[0].'.png'; 
     system($commandLine,$return); 
     if ($return==0){

     	//
	//write date
	//
	$command='mogrify -fill red -gravity south  -draw "text 0,0 '.$date.' " -pointsize 20 '.$aux[0].'.png';	
        system($command,$return); 
	if($return >0){
	echo '<p> mogrify error  </p>';
	}    
    }
     else{
     echo '<p> convert error '.$commandLine.' </p>';
     }
     $name[$k]=$aux[0].'.png';
     $k=$k+1;
     $i=$i-1;
      
  }
  return $name;
 }
 else
 {
   echo '<p> no picture </p>';
   return false;
 }
}


// echo "mogrify -fill yellow -gravity north -draw 'text 0,0 ".'"'.$date.'"'."' -pointsize 20 CCDImage64.png ";
function getFiles_test($path){
 system('ls -tr '.$path.' > files.txt',$return); 
 if ($return==0){
 $test=file('files.txt', FILE_IGNORE_NEW_LINES);
 $length=count($test);
 $j=$length-3;
 $i=$length-1;
 $date=getHeader($path.$test[$i]);
 echo '<p> date: '.$date.'! </p>';

 $aux=explode('.',$test[$i]);
$commandLine='convert -verbose -normalize -geometry 512x512 '.$path.$test[$i].' '.$aux[0].'.png'; 
exec($commandLine,$return); 


$command='mogrify -fill red -gravity south  -draw "text 0,0 '.$date.' " -pointsize 20 '.$aux[0].'.png';	
        system($command,$return); 

}

}
 ?>