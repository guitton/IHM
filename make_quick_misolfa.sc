#!/bin/bash
# Appel pour generer le quickook des acquisitions de MISOLFA
# pour la voie image: utilise la derniere image quick.fits produite par
echo "----- DEBUT quick_misolfa.sc `date` -----" 
cd /home/renaud/trac/centraliseur/ADMIN/
. ./env.shrc
postfix=$$
#
# on verifie que pcp-picard-host2l est allumee
ping -c2 misolfa4l >& $TMP/qm_etat_qm1_"$postfix".txt
grep 'Destination Host Unreachable' $TMP/qm_etat_qm1_"$postfix".txt
grep 'Destination Host Unreachable' $TMP/qm_etat_qm1_"$postfix".txt > $TMP/qm_etat2_qm1_"$postfix".txt
ping -c2 misolfa3l >& $TMP/qm_etat_qm1_"$postfix".txt
grep 'Destination Host Unreachable' $TMP/qm_etat_qm1_"$postfix".txt
grep 'Destination Host Unreachable' $TMP/qm_etat_qm1_"$postfix".txt > $TMP/qm_etat3_qm1_"$postfix".txt
cp /dev/null  $TMP/qm_xqm1_"$postfix".txt
#
#################################################################
# Si les machine misolfa4l et misolfa3l sont eteintes ...
#################################################################
if [ -s $TMP/qm_etat2_qm1_"$postfix".txt ] && [ -s $TMP/qm_etat3_qm1_"$postfix".txt ]
then  
  convert -normalize -geometry 512x512 quickp.fits quick2.png
  datobs=`/home/renaud/bin/listheader quickp.fits|grep DATE|sed 's/DATE_OBS= .//'|sed 's/... date of observation (TU)//'`
  echo "mogrify -fill white -gravity north  -draw 'text 10,10  \"$datobs\"' -pointsize 18 quick2.png" > $TMP/qm_xqm1_"$postfix".txt
  echo "mogrify -fill white -gravity south  -draw 'text 10,10  \"misolfa4l et misolfa3l OFFLINE\"' -pointsize 18 quick2.png" >> $TMP/qm_xqm1_"$postfix".txt
  chmod +x $TMP/qm_xqm1_"$postfix".txt
  $TMP/qm_xqm1_"$postfix".txt
  cp quick2.png quick.png
  cp quick.png /var/www/picardsol/
  echo "Depot sur rossini sodism2/quick.png"
  ping -c2 rossini > $TMP/qm_rossini_qm1_"$postfix".txt
  grep  'Destination Host Unreachable' $TMP/qm_rossini_qm1_"$postfix".txt > $TMP/qm_rossini2_qm1_"$postfix".txt
  if [ ! -s $TMP/qm_rossini2_qm1_"$postfix".txt ] 
  then
    echo "cd webrenaud" > $TMP/qm_put_rossini_qm1_"$postfix".io
    echo "cd sodism2" >>  $TMP/qm_put_rossini_qm1_"$postfix".io
    echo "put quick.png" >>  $TMP/qm_put_rossini_qm1_"$postfix".io
    sftp rossini < $TMP/qm_put_rossini_qm1_"$postfix".io > /dev/null
    rm $TMP/qm*_"$postfix".*
    exit
  fi
  rm  $TMP/qm*_"$postfix".*
  echo " - misolfa4l eteinte"
  echo "misolfa4l OFFLINE" >$TMP/SERVEUR/quicklast.txt
  cd /home/renaud/trac/centraliseur/ADMIN
  echo "mogrify -fill red -gravity center  -draw 'text 20,-15 \"misolfa4l eteinte \"' -pointsize 18 quick.png" >> $TMP/qm_xqm1_"$postfix".txt
  echo " - misolfa3l eteinte"
  echo "misolfa3l OFFLINE" >$TMP/SERVEUR/quicklast.txt
  info=`cat  $TMP/SERVEUR/quicklast.txt`
  echo "mogrify -fill red  -gravity center  -draw 'text 20,+15 \"misolfa3l eteinte \"' -pointsize 18 quick.png" >> $TMP/qm_xqm1_"$postfix".txt
  new=`diff $TMP/qm_xqm1_"$postfix".txt  $TMP/qm_xqm1_"$postfix".txt.sauv`
   if [ ! -n "$new" ]
   then
    echo "image web inchangee: on sort"
    rm $TMP/qm*_"$postfix".*
    exit
  fi
  chmod +x $TMP/qm_xqm1_"$postfix".txt
  $TMP/qm_xqm1_"$postfix".txt
  cp $TMP/qm_xqm1_"$postfix".txt $TMP/qm_xqm1_"$postfix".txt.sauv
  cp quick.png /var/www/picardsol/
  echo "Depot sur rossini sodism2/quick.png"
  ping -c2 rossini > $TMP/qm_rossini_qm1_"$postfix".txt
  grep  'Destination Host Unreachable' $TMP/qm_rossini_qm1_"$postfix".txt > $TMP/qm_rossini2_qm1_"$postfix".txt
  if [ ! -s $TMP/qm_rossini2_qm1_"$postfix".txt ] 
  then
   echo "cd webrenaud" > $TMP/qm_put_rossini_qm1_"$postfix".io
   echo "cd sodism2" >>  $TMP/qm_put_rossini_qm1_"$postfix".io
   echo "put quick.png" >>  $TMP/qm_put_rossini_qm1_"$postfix".io
   sftp rossini < $TMP/qm_put_rossini_qm1_"$postfix".io > /dev/null
  fi
  rm $TMP/qm*_"$postfix".*
  exit # car misolfa3 ET misolfa4l sont OFFLINE 
fi
#################################################################
#
# misolfa3 ou misolfa4l sont online 
#
#################################################################
#
# traitement misolfa4l :
#
if [ -s $TMP/qm_etat2_qm1_"$postfix".txt ] 
 then
  echo " - misolfa4l eteinte"
  echo "misolfa4l OFFLINE" >$TMP/SERVEUR/quicklast.txt
  cd /home/renaud/trac/centraliseur/ADMIN/
  echo "mogrify -fill red -gravity center  -draw 'text 20,-15 \"misolfa4l eteinte \"' -pointsize 18 quick.png" > $TMP/qm_xqm1_"$postfix".txt
else # Sinon traitement image misolfa4
#################################################################
 echo "traitement image misolfa4l"
 dir=`date +20%y%m%d`
 . ./env.shrc
 echo "cd $dir" > $TMP/qm_q_"$postfix".txt
 echo "ls PIC*fits" >> $TMP/qm_q_"$postfix".txt
 ./ftp_misolfa4.sc < $TMP/qm_q_"$postfix".txt|grep fits> $TMP/qm_qqq_"$postfix".txt
 if [ -e $TMP/qm_qqq_"$postfix".txt ]
 then
  sort -k8 $TMP/qm_qqq_"$postfix".txt|tail -1|awk '{print $9}' > $TMP/SERVEUR/quicklast.txt 
 else
   echo "misolfa4l ON LINE" > $TMP/SERVEUR/quicklast.txt 
   echo "misolfa4l ON LINE" > $TMP/SERVEUR/quick.txt
 fi
 cd /home/renaud/trac/centraliseur/ADMIN/decom_sodism2/exec
 echo "cd QUICK" > $TMP/qm_q_"$postfix".txt
 echo "bin" >> $TMP/qm_q_"$postfix".txt
 echo "lcd $TMP/SERVEUR/" >> $TMP/qm_q_"$postfix".txt
 echo "get quick.fits quickp.fits" >> $TMP/qm_q_"$postfix".txt
 echo "cd ../LOG" >> $TMP/qm_q_"$postfix".txt
 echo "get misolfa_"$dir".txt misolfa.log"  >> $TMP/qm_q_"$postfix".txt
 cat $TMP/qm_q_"$postfix".txt
 ftp misolfa4l < $TMP/qm_q_"$postfix".txt
 ls -l $TMP/SERVEUR/quickp.fits 
 #grep 'E S P A C E' /home/TEMP/SERVEUR/misolfa.log
 sizeq=`ls -l "$TMP/SERVEUR/quickp.fits"| awk '{print $5}'`
 echo "sizeq = $sizeq"
 if [ "$sizeq" = "1232640" ]
 then
  echo "cp $TMP/SERVEUR/quickp.fits $TMP/SERVEUR/quick.fits"
  cp $TMP/SERVEUR/quickp.fits $TMP/SERVEUR/quick.fits
  cp $TMP/SERVEUR/quickp.fits /home/renaud/trac/centraliseur/ADMIN /
  quicksave=`/home/renaud/bin/listheader $TMP/SERVEUR/quickp.fits|grep DATE_OBS|awk '{print $2}'|sed 's/^.//'|sed 's/.$//'|sed 's/-//g'|sed 's/T/_/'|sed 's/://g'`
  dirquicksave=`/home/renaud/bin/listheader $TMP/SERVEUR/quickp.fits|grep DATE_OBS|sed 's/T/ /2'|awk '{print $2}'|sed 's/^.//'|sed 's/-//g'`
  if [ ! -e  /DATA/PupilleData/images/"$dirquicksave" ]
  then
   mkdir /DATA/PupilleData/images/"$dirquicksave"
  fi
  # si le mkdir a echoue on recopie sur home le dossier
  if [ ! -e  /DATA/PupilleData/images/"$dirquicksave" ]
  then
   cp $TMP/SERVEUR/quickp.fits ./image_"$quicksave".fits
  else
   echo  "cp $TMP/SERVEUR/quickp.fits /DATA/PupilleData/images/$dirquicksave/image_$quicksave.fits"
   cp $TMP/SERVEUR/quickp.fits  /DATA/PupilleData/images/"$dirquicksave"/image_"$quicksave".fits
  fi
 echo "convert -normalize -geometry 512x512 $TMP/SERVEUR/quick.fits quick.png"
 convert -normalize -geometry 512x512 $TMP/SERVEUR/quick.fits quick.png
 datobs=`/home/renaud/bin/listheader $TMP/SERVEUR/quick.fits|grep DATE|sed 's/DATE_OBS= .//'|sed 's/... date of observation (TU)//'`
 echo "mogrify -fill white -gravity north  -draw 'text 10,10  \"$datobs\"' -pointsize 14 quick.png" > $TMP/qm_xqm1_"$postfix".txt
 DD=`egrep 'E S P|Espace' /home/TEMP/SERVEUR/misolfa.log|tail -1`
 egrep 'E S P|Espace' /home/TEMP/SERVEUR/misolfa.log|tail -1
 echo "mogrify -fill white -gravity north  -draw 'text 10,30  \"$DD\"' -pointsize 14 quick.png" >> $TMP/qm_xqm1_"$postfix".txt
 info=`cat  $TMP/SERVEUR/quicklast.txt`
 # datation acq voie image
date +%H
 hcour=`date +%H`
date +%M
 mncour=`date +%M`
 hmcour=`expr $hcour \* 60`
 hmcour=`expr $hmcour  + $mncour `
 echo "hcour=$hcour $mncour $hmcour "
 date
 hms=`cat  $TMP/SERVEUR/quicklast.txt|sed 's/_/ /g'|awk '{print $7}'`
 hcour=`echo ${hms:0:2}`
 mncour=`echo ${hms:2:2}`
 hmold=`expr $hcour \* 60 + $mncour `
 hmcour=`expr $hmcour - $hmold`
 if [ "$hmcour" -gt 14 ] # > 12mn -> arret acq  misolfa4 ?
 then
  echo "cp $TMP/SERVEUR/quickp.fits $TMP/SERVEUR/quick.fits"
  cp $TMP/SERVEUR/quickp.fits $TMP/SERVEUR/quick.fits
  convert -normalize -geometry 512x512 $TMP/SERVEUR/quick.fits quick.png
  echo "mogrify -fill orange -gravity south  -draw 'text 10,55  \"$info\"' -pointsize 14 quick.png" >> $TMP/qm_xqm1_"$postfix".txt
 else
  echo "mogrify -fill white -gravity south  -draw 'text 10,55  \"$info\"' -pointsize 14 quick.png" >> $TMP/qm_xqm1_"$postfix".txt
 fi
 
 else
  echo "!!! misolfa4l quick.fits non valable"
  info="!!! misolfa4l quick.fits non valable"
  echo "mogrify -fill orange -gravity south  -draw 'text 10,55  \"$info\"' -pointsize 14 quick.png" >> $TMP/qm_xqm1_"$postfix".txt
 fi
fi
###########################################
#
# traitement misolfa3l :
#
cat  $TMP/qm_etat3_qm1_"$postfix".txt
if [ -s $TMP/qm_etat3_qm1_"$postfix".txt ] # misolfa3l OFFLINE
then
 echo " - misolfa3l eteinte!   "
 echo "misolfa3l OFFLINE" >$TMP/SERVEUR/quicklast.txt
 info=`cat  $TMP/SERVEUR/quicklast.txt`
 echo "mogrify -fill red -gravity south  -draw 'text 10,32  \"misolfa3l OFFLINE\"' -pointsize 14 quick.png" >> $TMP/qm_xqm1_"$postfix".txt
else
 echo "acces misolfa3l"
 ymj=`date +%Y%m%d`
 datas="false"
 #
 # dans un 1er temps on va voir si la versiom Mammar tourne:
 # 
 echo "cd PupilleData"  > $TMP/qm_lsp2_"$postfix".txt
 echo "ls"  >> $TMP/qm_lsp2_"$postfix".txt
 echo "ftp_misolfa3.sc"
 /home/renaud/trac/centraliseur/ADMIN/ftp_misolfa3.sc < $TMP/qm_lsp2_"$postfix".txt>  $TMP/qm_reslsp2_"$postfix".txt 
 echo "retour ftp_misolfa3.sc"
 mois=`date +%m|sed 's/01/Jan/'|sed 's/02/Feb/'|\
       sed 's/03/Mar/'|sed 's/04/Apr/'|sed 's/05/May/'|\
       sed 's/06/Jun/'|sed 's/07/Jul/'|sed 's/08/Aug/'|sed 's/09/Sep/'|\
       sed 's/10/Oct/'|sed 's/11/Nov/'|sed 's/12/Dec/'`
 jour=`date +%d`
 info=`grep "$mois $jour " $TMP/qm_reslsp2_"$postfix".txt|tail -1 |awk '{print $8"TU "$9}'`
 # on a recupere un fichier du jour en cours version Mammar
 if [ "$info" != "" ]
 then
  datas="true"
  hcour=`date +%H`
  mncour=`date +%M`
  hmcour=`expr $hcour \* 60`
  hmcour=`expr $hmcour  + $mncour `
  hcour=`echo ${info:0:2}`
  mncour=`echo ${info:3:2}`
  hmold=`expr $hcour \* 60 + $mncour `
  info="$ymj $info"
  hmcour=`expr $hmcour - $hmold`
  if [ "$hmcour" -gt 5 ]
  then
   echo "mogrify -fill orange"
   echo "hmcour=$hmcour hmold=$hmold"
   grep "$mois $jour " reslsp2.txt|sort -k 8
   echo "mogrify -fill orange -gravity south  -draw 'text 10,25  \"$info\"' -pointsize 14 quick.png" >> $TMP/qm_xqm1_"$postfix".txt
  else
   echo "mogrify -fill white -gravity south  -draw 'text 10,25  \"$info\"' -pointsize 14 quick.png" >> $TMP/qm_xqm1_"$postfix".txt
  fi
 fi
 #
 # dans un 2nd temps on va voir si la version Alexis tourne:
 # 
 info='misolfa3l ONLINE'
 echo "cd tests/observations" > $TMP/qm_lsp_"$postfix".txt
 echo "cd `date +20%y%m%d`" >> $TMP/qm_lsp_"$postfix".txt
 echo "ls" >> $TMP/qm_lsp_"$postfix".txt
 /home/renaud/trac/centraliseur/ADMIN/ftp_misolfa3.sc < $TMP/qm_lsp_"$postfix".txt|grep  'PIC_PUPILLE_20'|tail -1|grep -v 'not found'|awk '{print $9}'>  $TMP/qm_reslsp_"$postfix".txt
 cat $TMP/qm_reslsp_"$postfix".txt
 info=`cat $TMP/qm_reslsp_"$postfix".txt|tail -1`
 if [ -s  $TMP/qm_reslsp_"$postfix".txt ]
  then
   datas="true"
   if2=`cat $TMP/qm_reslsp_"$postfix".txt|tail -1`
   hms=`cat $TMP/qm_reslsp_"$postfix".txt|sed 's/_/ /g'|awk '{print $4}'`  
   hcour=`date +%H`
   mncour=`date +%M`
   hmcour=`expr $hcour \* 60`
   hmcour=`expr $hmcour  + $mncour `
   hcour=`echo ${hms:0:2}`
   mncour=`echo ${hms:2:2}`
   hmold=`expr $hcour \* 60 + $mncour `
   hmcour=`expr $hmcour - $hmold`
   if [ "$hmcour" -gt 20 ]
   then
   echo "mogrify -fill red -gravity south  -draw 'text 10,40  \"$if2\"'  -pointsize 14 quick.png" >> $TMP/qm_xqm1_"$postfix".txt
   else
    if [ "$hmcour" -lt 5 ]
    then
     echo "mogrify -fill white -gravity south  -draw 'text 10,40  \"$if2\"'  -pointsize 14 quick.png" >> $TMP/qm_xqm1_"$postfix".txt
    else 
     echo "mogrify -fill orange -gravity south  -draw 'text 10,40  \"$if2\"'  -pointsize 14 quick.png" >> $TMP/qm_xqm1_"$postfix".txt
    fi
   fi
 fi
fi
#########################################
#
# recuperation du filtre en cours:
 iconv -f ISO-8859-15 -t UTF-8 /home/journaux_pilotage_misolfa/JournalMISOLFA"`date +20%y%m%d`".txt|grep 'filtres en position' |tail -1|awk '{print $9}'> $TMP/qm_resfiltr_"$postfix".txt
 filtre=`cat $TMP/qm_resfiltr_"$postfix".txt`
 if [ -s  $TMP/qm_resfiltr_"$postfix".txt ]
  then
   filtre=`cat $TMP/qm_resfiltr_"$postfix".txt`
   info=`echo "(filtre $filtre)"`
    echo "mogrify -fill white -gravity south  -draw 'text 10,10  \"$info\"'  -pointsize 14 quick.png" >> $TMP/qm_xqm1_"$postfix".txt
  fi
  rm $TMP/qm_resfiltr_"$postfix".txt
 # pas d'acquisitions en cours:
 if [ "datas" = "false" ]
 then
  echo "mogrify -fill white -gravity south  -draw 'text 10,40  \"misolfa3l ONLINE\"'  -pointsize 14 quick.png" > $TMP/qm_xqm1_"$postfix".txt
 fi
chmod +x $TMP/qm_xqm1_"$postfix".txt
cat $TMP/qm_xqm1_"$postfix".txt
$TMP/qm_xqm1_"$postfix".txt
cp quick.png /var/www/picardsol/
ping -c2 rossini > $TMP/qm_rossini_qm1_"$postfix".txt
grep  'Destination Host Unreachable' $TMP/qm_rossini_qm1_"$postfix".txt > $TMP/qm_rossini2_qm1_"$postfix".txt
if [ ! -s $TMP/qm_rossini2_qm1_"$postfix".txt ] 
then
 echo "cd webrenaud" > $TMP/qm_put_rossini_qm1_"$postfix".io
 echo "cd sodism2" >>  $TMP/qm_put_rossini_qm1_"$postfix".io
 echo "put quick.png" >>  $TMP/qm_put_rossini_qm1_"$postfix".io
 echo "Depot sur rossini sodism2/quick.png"
 sftp rossini < $TMP/qm_put_rossini_qm1_"$postfix".io > /dev/null
fi
rm $TMP/qm*_"$postfix".*
echo "----- FIN quick_misolfa.sc `date` -----"
exit
