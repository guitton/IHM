<?php 
global $jour;
error_reporting(E_ALL ^ E_DEPRECATED);
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
if ((isset($_GET['jj'])) && (isset($_GET['mm'])) && (isset($_GET['yy'])))
{
   $jour= $_GET['jj'].'-'. $_GET['mm'].'-'. $_GET['yy'] ;
   $jour2=$_GET['yy'].'-'. $_GET['mm'].'-'. $_GET['jj'];
   $yy=$_GET['yy'];
   $mm=$_GET['mm'];
   $jj=$_GET['jj'];
   $jour3=$yy.$mm.$jj ;
   $jjul=strtotime($jour2);
   $jourm=date("d/m/Y",$jjul+24*3600);
   $jour3m=date("Ymd",$jjul+24*3600);
}
else 
{
  $jour=date("d-m-Y",time()-3600*24);
  $jourm=date("d-m-Y",time());
  $jour2=date("Y-m-d",time()-3600*24);
  $jour3=date("Ymd",time()-3600*24);
  $jour3m=date("Ymd",time());
  $yy=date("Y",time()-3600*24);
  $mm=date("m",time()-3600*24);
  $jj=date("d",time()-3600*24);
}
$nb_obs=0;
include "graph_data.php";
//echo $graph1_data."<br>";echo '<br>';
//echo $graph222_data;echo '<br>';echo '<br>';echo '<br>';
//echo $graphdome_data."<br>";echo '<br>';
//echo $graph33_data;echo '<br>';echo '<br>';echo '<br>';

//echo $graph22_data;
//echo '../sbig/first_allsky_'.$jour3.'.jpg';
?>

<head> 
   <meta charset="UTF-8" />
   <!-- Des choses -->
   <meta name="viewport" content="width=device-width"/>
	<link rel="stylesheet" type="text/css" href="cats.css">
	<link rel="stylesheet" type="text/css" href="cats_gdimm.css">
	<link rel="stylesheet" type="text/css" href="cats_allsky.css">
</head>

<body>

<center>
C A T S (Calern Atmospheric Turbulence Station)
<?php 
if ( $nb_obs == 2 ) {
 echo '<br><br>'.$jour.' : NO OBSERVATIONS';
 }
else {
 echo '<br>'.$jour.'<br>';
 //echo "Nombre d'observations=".$nb_obs.'<br>';
}
?>
</center>

<center>

<table>
<td><br>
<center>Meteo Data 
 <div  id="graph_humidity" style="width: 340px; height: 210px"></div>
 <div  id="graph_temp" style="width: 340px; height: 210px"></div>
 <div  id="graph_windspeed" style="width: 340px; height: 210px"></div>
</center>
</td>

<td><br>
<div style="position:relative top:20px; "> </div>
<center>AllSky Data
<div id="graph_cloudy" style="width: 340px; height: 210px"></div>
<?php
function UR_exists($url){
   $headers=get_headers($url);
   return stripos($headers[0],"200 OK")?true:false;
}
echo '<div style=" ">';
if ( $nb_obs > 2 ) {
$mediane_epsT=($mediane_epsT+$mediane_epsL)/2;
echo '<font size="2"><br>Median values:<br>Seeing = ';
printf("%.02f",$mediane_epsT);
echo "&quot;<br>Scintillation = ";
printf("%.01f",$mediane_33);
echo "%<br>Isoplanetisme = ";
printf("%.02f",$mediane_333);
echo "&quot;</font>";
}
else {
echo '<font size="2"><br><br><br><br><br></font>';
}
echo '</div>';
echo '<div style="">';
$jourfits=$jour2;
if (UR_exists("https://cats-frontal.oca.eu/ALLSKY/AllSky_Fits_".$jourfits.".mp4")) {
echo '<font size="1">'.'<br> ALLSKY animation (click on -> mp4)<br></font>';
echo '<a href="https://cats-frontal.oca.eu/ALLSKY/AllSky_Fits_'.$jourfits.'.mp4">';
echo '<img src="https://cats-frontal.oca.eu/ALLSKY/AllSky_Fits_'.$jourfits.'.gif"  style="width: 310px;height:310px;"></a>';
}
else {
 if (UR_exists("https://cats-frontal.oca.eu/ALLSKY/AllSky_Fits_".$jourfits.".gif")) {
  echo '<font size="1">'.'<br>ALLSKY animation <br></font>';
  echo '<a href="https://cats-frontal.oca.eu/ALLSKY/AllSky_Fits_'.$jourfits.'.gif">';
  echo '<img src="https://cats-frontal.oca.eu/ALLSKY/AllSky_Fits_'.$jourfits.'.gif"  tyle="width: 310px;height:310px;"></a>';
 }
 else {
  echo '<font size="1">'."<br>First image of ".$jourm."<br></font>";
  echo '<img src="https://cats-frontal.oca.eu/sbig/first_allsky_'.$jour3m.'.jpg"  style="width:310px;height:310px;" alt="NO IMAGE FOR THIS DAY">';
 }
}
echo '</div>';
?>
</center>


<?php 
//if ( $nb_obs > 2 ) {
echo "<td><br><center>GDIMM Data<center>";
echo '<div  id="graph_r0" style="width: 340px; height: 210px"></div>';
echo '<div  id="graph_ind" style="width: 340px; height: 210px"></div>';
echo '<div  id="graph_isop" style="width: 340px; height: 210px"></div></td>';
//}
?>

</table>  

</center>


</body>
