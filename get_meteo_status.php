<?php
include "config.php"; 


function getmeteoCurve($CurveName)
{
 global $db;
 global $datedeb;
 global $datefin;
 // dates debut et fin de nuit :
 $datedeb="";
 $datefin="";
 // date_default_timezone_set("Europe/Paris"); fait dans php.ini
 getDates(); // ->  $datedeb  et $datefin
 $sql = "SELECT * FROM `cats_weather` where timestamp >= '". $datedeb . "' and timestamp <= '". $datefin .  "'"  ;
//echo $sql.'<br>';
 $result = "";
 $req = mysqli_query($db,$sql) or die('Erreur SQL !<br>'.$sql.'<br>'.mysqli_error());
 $cpt = 0;
 $status_id=0; 
 $timestamp=0;
 while($data = mysqli_fetch_assoc($req)) { 	
  $status_idprec=$status_id; 
  $timestampprec = $timestamp;
  $status_id = $data['status_id'];
  $status_id = $status_id*5;
  $timestamp = strtotime($data['timestamp']) * 1000;
  if ($cpt == 0) { 
   $result = $result. " [".strtotime($datedeb) * 1000 .", null],"; 
   if ($status_id > 5) {  
   $result = $result. " [".strtotime($datedeb) * 1000 .", 5],"; 
    $result = $result. " [".$timestamp .",5], ";
   }
   $result = $result. " [".$timestamp .",".$status_id."]";
  } 
  if ($cpt > 0)  {  
    $timestampprec = $timestamp-1;
    $result = $result. ", [ ".$timestampprec .", ".$status_idprec." ],";
    $result = $result. " [ ".$timestamp.", ".$status_id." ]";
  }
  $cpt++;
 }
 if ($cpt > 0) {   
    $timestampprec = $timestamp+1;
    $result = $result. ",[".$timestampprec.",".$status_id."],";
    if ($status_id < 10) {
     $result = $result. "[".strtotime($datefin) * 1000 . ",5]";
    } else {
     $result = $result. "[".strtotime($datefin) * 1000 .", null ]";
    }
 }

 //echo "result= ".$result. "<br>"  ;
 return $result;
}

function getfinalmeteoCurve($tab_name)
{
 // protect	
 $result  = getmeteoCurve($tab_name);
 $curves = "";
 if ( strcmp($result,$curves) !== 0  ) {	
  $curves = $curves."{type:'line',showInLegend:false, color:'#003366',tooltip: {headerFormat: ' ',pointFormat: '{point.x:%e. %b %H:%M:%S}'}, name:'',".sprintf("data:[%s]}",$result);
 }
 return $curves;
}


$graphdome_data= "";

// DATA FOR GRAPHIC
$db = mysqli_connect($host, $login, $pass,'cats'); 
if ($db) {
 $tab_name =  array ("METEO");
 $graphdome_data= getfinalmeteoCurve($tab_name);
 mysqli_close($db); 
}
?>
