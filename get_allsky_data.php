<?php
include "config.php";
/////////////////////////////////
function getDataCurve($CurveName)
{
 global $db;
 global $datedeb;
 global $datefin;
 global $cpt;
 // dates debut et fin de nuit :
 $datedeb="";
 $datefin="";
 getDates(); // ->  $datedeb  et $datefin
 $sql = "SELECT * FROM `cats_allsky` where timestamp >= '". $datedeb . "' and timestamp <= '". $datefin .  "'"  ;
 //echo $sql.'<br>';
 $result = "";  
 $cpt = 0;
 $req = mysqli_query($db,$sql) or die('Erreur SQL !<br>'.$sql.'<br>'.mysqli_error());
 while($data = mysqli_fetch_assoc($req)) { 
   $value = $data['value'];
   // pour la ALLSKY le timestamp Mysql est de type NOW() soit :
   // $data['timestamp'] =AAAA-MM-DD HH:MM:SS
   $timestamp = (strtotime($data['timestamp']))*1000; 
   //$timestamp : timestamp Unix (le nombre de secondes depuis le 1er Janvier 1970 � 00:00:00 UTC) 
   //             *1000 pour Highcharts
   //Highcharts uses javascript Date numbers (the number of milliseconds since Jan 1st 1970)
   if ($cpt == 0) {
    $result = $result." [".((strtotime($datedeb))*1000).", null ],";
    $result = $result. " [". $timestamp .",".$value."]";
   } else {
    $result = $result. ", [". $timestamp .",".$value."]";
   }
   $cpt++;  
  } 
  if ($cpt == 0) {
   $result = "[".((strtotime($datedeb))*1000).", null], [".((strtotime($datefin))*1000).", null]";
  } else {
   $result = $result.", [".((strtotime($datefin))*1000).", null]";
  }
 return $result;
}

function getFinalCurve($tab_name)
{
 // protect
 global $graph_cloudymax; // seuil cloudy
 global $cpt;
 global $cloudy_max;
 for($i = 0; $i < count($tab_name); ++$i) {
  $CurveName = $tab_name[$i];
  $result  = getDataCurve($CurveName);
  $tab_title[] = $CurveName;
  $tab_donnee[] = $result;
 }
 $curves = "series:[";
 for($i = 0; $i < count($tab_title); ++$i) {
    $curves = $curves. sprintf("{showInLegend:false,  name:'%s',data:[%s]}",$tab_title[$i] ,$tab_donnee[$i]);
 }
 if ($cpt == 0) {
   $curves = $curves .' ,'. $graph_cloudymax . "], labels: {
   items:[{html:'Cloudy threshold : ". $cloudy_max ."%',style: {left:'50%',top:'1px',color:'#55EEEE'}},{html:'NO DATA',style:{left:'80%',top:'50px',color:'red'}}]}";
 } else {
 $curves = $curves .' ,'. $graph_cloudymax . "], labels: {
   items:[{html:'Cloudy threshold : ". $cloudy_max ."%',style: {left:'50%',top:'1px',color:'#55EEEE'}}]}";
 }
 return $curves;
}
// Connexion et s�lection de la base
$db = mysqli_connect($host, $login, $pass,'cats');
if ( $db) {
 // GET DATA FOR GRAPHIC 
 $tab_name =  array ("Cloudy");  // nom de la table dans la DB
 $graph1_data= getFinalCurve($tab_name); // serie pour highcharts
 //echo $graph1_data;
 mysqli_close($db); 
}
?>
