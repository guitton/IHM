<?php
include "config.php"; 
$vide=empty($_POST);
if ( ! $vide) {
 $wm=$_POST['wind_max'];
 $cm=$_POST['cloudy_max'];
 $rm=$_POST['rainrate_max'];
 $hm=$_POST['humidity_max'];
 $dm=$_POST['dewpoint_max'];
 $db =mysqli_connect($host,$login, $pass,'cats');
 if (mysqli_connect_errno())
  {
   echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
 $result = mysqli_query ($db, "select * from cats_weather_pref where 1 ");
 while ($row = mysqli_fetch_assoc($result)) {
  $rm2=$row['rainrate_max'];
  $wm2=$row['wind_max'];
  $hm2=$row['humidity_max'];
  $cm2=$row['cloudy_max'];
  $dm2=$row['dewpoint_max'];
 }
 ////////////////////////////////
 if (($wm == $wm2 ) && ($cm == $cm2 ) && ($rm == $rm2 )  && ($hm == $hm2 ) && ($dm == $dm2 )){
  $vide='true';
  mysqli_close($db);
  //echo '<br>pas de changement demande<br>';
 }
 if( ! $vide) {
  // Le formulaire a ete envoye, on va tester les valeurs
  $maj='oui';
  if ($wm < 0) {
  echo "wind max =".$wm." ".$wm2."<br>";
  echo "wind max doit etre > 0 , pas de changement!</br>";
  $maj='non';
  }
 if ($cm < 0) {
  echo "cloudy max =".$cm." ".$cm2."<br>";
  echo "wind max doit etre > 0 , pas de changement!</br>";
  $maj='non';
 }
 if ($hm < 0) {
  echo "humidity max =".$hm." ".$hm2."<br>";
  echo "humidity max doit etre > 0 , pas de changement!</br>";
  $maj='non';
 }
 if ($rm < 0) {
  echo "rainrate max =".$rm." ".$rm2."<br>";
  echo "rainrate max doit etre > 0 , pas de changement!</br>";
  $maj='non';
 }
 if (! is_numeric($dm)) {
  echo $dm." n'est pas de type numérique (exemples acceptes:2 ou 2.2)<br>";
  $maj='non';
 }
 ////////////////////////
 if ($maj == 'oui'){
  $query="UPDATE cats_weather_pref SET  cloudy_max='$cm', humidity_max='$hm', rainrate_max='$rm', wind_max='$wm', dewpoint_max='$dm'  WHERE 1";
  $result=true;
  $result=mysqli_query($db,$query); 
  if (!$result) {
    die('Requête invalide : ' . mysqli_error());
  }
  else {
   echo "<br>Updated<br>";
  }
  }
  mysqli_close($db);
  // trace du query dans fichier log
  $file = '/home/LOG/cats_config.log';
  $dd= gmdate("Ymj H:i:s");
  $txt="(TU)";
  if ($wm != $wm2 ) {
   $txt=$txt." wind_max='$wm'";
  }
  if ($cm != $cm2 ){
   $txt=$txt." cloudy_max='$cm'";
  }
  if  ($rm != $rm2 ) {
   $txt=$txt." rainrate_max='$rm'";
  }
  if  ($hm != $hm2 ) {
   $txt=$txt." humidity_max='$rm'";
  }
  if  ($dm != $dm2 ) {
   $txt=$txt." dewpoint_max='$dm'";
  }
  file_put_contents($file, $dd.$txt."\r\n",FILE_APPEND);
  echo "<br>". $dd.$txt."<br>";
 }
}
else {
 $db =mysqli_connect($host,$login, $pass,'cats' );
 if (mysqli_connect_errno())
  {
   echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

 $result = mysqli_query ($db,"select * from cats_weather_pref  where 1 limit 0,30");
 while ($row = mysqli_fetch_assoc($result)) {
  $cm=$row['cloudy_max'];
  $hm=$row['humidity_max'];
  $rm=$row['rainrate_max'];
  $wm=$row['wind_max'];
  $dm=$row['dewpoint_max'];
 }
 mysqli_free_result($result);
 mysqli_close($db);

// Entete de la page HTML
echo '<html><head>';
echo '<center>C A T S<br>Mises &agrave; jour des parametres de config METEO</center>'; 
 echo '<form action="cats_config_v1.php" method="post">';
 echo '    cloudy max :<input type="number" name="cloudy_max" value='.$cm.' ><br>';
 echo '    humidity max :<input type="number" name="humidity_max" value='.$hm.' ><br>';
 echo '    rainrate max :<input type="number" name="rainrate_max" value='.$rm.' ><br>';
 echo '    wind max :<input type="number" name="wind_max" value='.$wm.' ><br>';
 echo '    dewpoint :<input type="text"  name="dewpoint_max" value='.$dm.' ><br>';
 echo '    <input type="submit" value="Update" ><br>';
 echo '</form> ';
 echo '</body></html>'; 
}
?>
