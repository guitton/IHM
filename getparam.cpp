// pour compiler seul : main decommente
// g++ -I ./ getparam.cpp -> a.out
#include <cstdio>
#include <cstdlib>
#include <string>
//#include <basic_string.h>
#include <iostream>
#include <fstream>

using namespace std;
template class std::basic_string<char>;
template class std::basic_ifstream<char>;
template class std::basic_istream<char>;
template class std::basic_ostream<char>;
template class std::basic_ios<char>;

#include <getparam.h>

void tracer(int res);

int getparam()
{  
  string line;
  string myStr;
  size_t pos1;
  int size;
  string param,valeur;
  int res;
  ifstream myfile;
  ////////////////////////////////////////////////////////////
  res=0;
  ///////////////////////////////////////////////////////////
  myfile.open ("./fichier.config");
  if (myfile.is_open()) {
    while (getline (myfile,line)) {
      pos1 = line.find("=",0);
      if ( pos1 < line.size() )
	{
	  param=line.substr(0,pos1);
	 size=line.size();
	  valeur=line.substr(pos1+1,size-pos1);
   
	  if (param.compare(0,8,"pointage") == 0){
	    pointage=valeur.c_str();
	  }
	  if (param.compare(0,12,"acquisitions") == 0){
	    acquisitions=valeur.c_str();
	  }
	  if (param.compare(0,4,"plan") == 0){
	    plan=valeur.c_str();
	  }
	  if (param.compare(0,7,"cameras") == 0){
	    cameras=valeur.c_str();
	  }
	  if (param.compare(0,9,"setcamera") == 0){
	    setcamera=valeur.c_str();
	  }
	  if (param.compare(0,11,"etatmonture") == 0){
	    etatmonture=valeur.c_str();
	  } 
          if (param.compare(0,4,"toit") == 0){
	    toit=valeur.c_str();
	  }
	  if (param.compare(0,8,"automate") == 0){
	    automate=valeur.c_str();
	  } 
	  if (param.compare(0,7,"secteur") == 0){
	    secteur=valeur.c_str();
	  } 
         if (param.compare(0,4,"alim") == 0){
   	    alim=valeur.c_str();
	  } 
	 if (param.compare(0,8,"pilotage") == 0){
	    pilotage=valeur.c_str();
	  }
	  if (param.compare(0,11,"acquisition") == 0){
	    acquisition=valeur.c_str();
	  }  
          if (param.compare(0,7,"filtre1") == 0){
	    filtre1=valeur.c_str();
	  }
	  if (param.compare(0,7,"filtre2") == 0){
	    filtre2=valeur.c_str();
	  } 
	  if (param.compare(0,4,"CCD1") == 0){
	    CCD1=valeur.c_str();
	  }
 	  if (param.compare(0,4,"CCD2") == 0){
	    CCD2=valeur.c_str();
	  }
	  if (param.compare(0,8,"Alphames") == 0){
	   Alphames=valeur.c_str();
	  }
	  if (param.compare(0,8,"Deltames") == 0){
	    Deltames=valeur.c_str();
	  }   
	  if (param.compare(0,10,"ecartalpha") == 0){
	    ecartalpha=valeur.c_str();
	  }   
	  if (param.compare(0,10,"ecartdelta") == 0){
	    ecartdelta=valeur.c_str();
	  }   
	  if (param.compare(0,8,"commande") == 0){
	   commande=valeur.c_str();
	  }   
	  if (param.compare(0,12,"etatcommande") == 0){
	    etatcommande=valeur.c_str();
	  }
	}
    }
    myfile.close();
  }
 return(res);
}
///////////////////////////////////////
void tracer()
{
    cout << "acquisitions = " << acquisitions << endl;
    cout << "pointage = " << pointage << endl;
    cout << "plan = " << plan << endl;
    cout << "cameras = " << cameras << endl;
    cout << "setcamera = " << setcamera << endl;
    cout << "secteur = " << secteur << endl;
    cout << "CCD1 = " << CCD1 << endl;
    cout << "acquisition = " << acquisition << endl;
    cout << "etatmonture = " << etatmonture << endl;
    cout << "automate = " << automate << endl;
    cout << "filtre2 = " << filtre2 << endl;
    cout << "pilotage = " << pilotage << endl;
    cout << "toit = " << toit << endl;
    cout << "alim = " << alim << endl;
    cout << "CCD2 = " << CCD2 << endl;
    cout << "filtre1 = " << filtre1 << endl;
    cout << "Deltames = " << Deltames << endl;
    cout << "Alphames = " << Alphames << endl;
    cout << "ecartalpha = " << ecartalpha << endl;
    cout << "ecartdelta = " << ecartdelta << endl;
    cout << "commande = " << commande << endl;
    cout << "etatcommande = " <<  etatcommande << endl; 
}
///////////////////////////////////////

/*int main()
{int res;
 bool tr;
 tr=true;
 //res=get_config(tr); 
 res=getparam();
 tracer();
 return(0);
 }*/

