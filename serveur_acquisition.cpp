#include <cstdio>
#include <cstdlib>
#include <cerrno>
#include <ctime>
#include <string.h> 
#include <string>
#include <sys/socket.h>
#include <arpa/inet.h> //inet_addr
#include <unistd.h> //write
#include <stdbool.h>
#include <pthread.h>
using namespace std;
////////////////////////
///////////////////////////////////////////
//
// compiler par : 
//  g++ -I ./  -c -o getparam.o getparam.cpp
//  gcc -Wall serveur_acquisition.cpp getparam.o -o serveur_acquisition -fno-builtin -lpthread
//g++ -I ./  -Wall serveur_acquisition.cpp getparam.cpp -o serveur_acquisition -fno-builtin -lpthread
///////////////////////////////////////////
#define PORT 10001   // The first 1000 ports are reserved for specific applications on linux
#define NUM_THREADS 1
struct thread_data {
   int  thread_id;
   char *message;
};

//extern string toit;

extern string acquisitions;
extern string cameras;
extern string alim;
extern string plan;
extern string pointage; 
extern string image;
// lecture des parametres :
extern int getparam();
///////////////////////
// Variables globales:
time_t t;
struct tm t_m;
int client_sock, read_size,  hms_fincor, hh, mn;
bool mess1, mess2;
bool breakreadwrite;
FILE *trace;
int status;

///////////////////////////////////////////////////////
void *readsocket(void *threadid){
 //
 int n1;
 char buffer[256];
 // char buffertrace[256];
 while (1) {
  bzero(buffer, 256); 
  read_size = recv(client_sock, buffer, 256, 0);
  if (read_size > 0 ){
   t = time(NULL);
   t_m = *localtime(&t);
   hms_fincor = t_m.tm_hour*60+t_m.tm_min;
   printf("Message recu = %s (hh=%d mn=%d %d sec)\n", 
	   buffer, t_m.tm_hour, t_m.tm_min, t_m.tm_sec)	;
   fflush(stdout);
   getparam(); // lecture des etats
   /* messages attendus:  
   $pointage
   $stopAcquisition
   $changePlan
   $imageAcquisition
   $cameraOn  (pluriel: donner numero ou toutes ensembles?. plusieures cameras?)
   $cameraOff
   $cameraEtat
   $alimOn
   $alimOff
   */ 
   n1=strncmp(buffer, "$pointage!", 10); //$pointage! la commande envoyée
    if (n1 == 0) {     
     fflush(stdout);
     bzero(buffer, 256);//erases data
     sprintf(buffer, "%s", pointage.c_str());  //besoin de connaitre la requete de depart?
     status = write(client_sock, buffer, strlen(buffer));	           
    } else {
     n1=strncmp(buffer, "$stopAcquisition!", 17); 
     if (n1 == 0) {     
      fflush(stdout);
      bzero(buffer, 256);
      sprintf(buffer, "%s",acquisitions.c_str());
      status = write(client_sock, buffer, strlen(buffer));	           
     } else {
      n1=strncmp(buffer, "$changePlan!", 12); 
      if (n1 == 0) {     
       fflush(stdout);
       bzero(buffer, 256);
       sprintf(buffer, "%s", plan.c_str());
       status = write(client_sock, buffer, strlen(buffer));	           
      } else {
        n1=strncmp(buffer, "$imageAcquisition!", 18); 
        if (n1 == 0) {     
         fflush(stdout);
         bzero(buffer, 256);
         sprintf(buffer, "$imageAcquisition:%s", image.c_str());	 
         status = write(client_sock, buffer, strlen(buffer));	            
        } else {
        n1=strncmp(buffer, "$cameraOff!", 11); 
        if (n1 == 0) {     
         fflush(stdout);
         bzero(buffer, 256);
         sprintf(buffer, "$cameraOff:%s!", cameras.c_str());	 
         status = write(client_sock, buffer, strlen(buffer));	            
        } else {
        n1=strncmp(buffer, "$cameraOn!", 10); 
        if (n1 == 0) {     
         fflush(stdout);
         bzero(buffer, 256);
         sprintf(buffer, "$cameraOn:%s!", cameras.c_str());	 
         status = write(client_sock, buffer, strlen(buffer));	            
        } else {
        n1=strncmp(buffer, "$cameraEtat!", 12); 
        if (n1 == 0) {     
         fflush(stdout);
         bzero(buffer, 256);
         sprintf(buffer, "$cameraEtat:%s!", cameras.c_str());	 
         status = write(client_sock, buffer, strlen(buffer));	            
        } else {
        n1=strncmp(buffer, "$alimOn!", 8); 
        if (n1 == 0) {     
         fflush(stdout);
         bzero(buffer, 256);
         sprintf(buffer, "$alimOn:%s!", alim.c_str());	 
         status = write(client_sock, buffer, strlen(buffer));	            
        } else {
        n1=strncmp(buffer, "$alimOff!", 9); 
        if (n1 == 0) {     
         fflush(stdout);
         bzero(buffer, 256);
         sprintf(buffer, "$alimOff:%s!", alim.c_str());	 
         status = write(client_sock, buffer, strlen(buffer));	            
        } else {			    
	   printf("message non reconnu %s\n", buffer);
	   fflush(stdout);
           bzero(buffer, 256);
           sprintf(buffer, "$notcommand!");
           status = write(client_sock, buffer, strlen(buffer));	  
 }
	}
	}
	}
	}
	}
      }
     }
    }
  
  }
   else {
    breakreadwrite=true;
    printf("Connection arretee de la part de l'automate' %d\n", read_size);
    fflush(stdout);
    close(client_sock);
    pthread_exit(NULL);
  }
 } // while (1)	
} // fin de readsocket
////////////////////////////////////////////
//
////////////////////////////////////////////
////////////////////////////////////////////
int main(int argc, char *argv[]) {
 int socket_desc, c;
 struct sockaddr_in server, client;
 char buffer[256];
 /* size_t buffer_size;
 char * word;
 char * pch1;
 char * pch2 ;*/
 int status;
 int hh,mn;
 // bool fichtrace, findujour;
 //////////////////////////////
 pthread_t threads[NUM_THREADS];
 struct thread_data td[NUM_THREADS];
 //////////////////////////////////
 t = time(NULL);
 t_m = *localtime(&t);
 printf("Debut de serveur acquisition a %dh%dmn%ds (%d/%d/%d) \n", 
	 t_m.tm_hour, t_m.tm_min, t_m.tm_sec, t_m.tm_mday, t_m.tm_mon+1, t_m.tm_year+1900);		
 fflush(stdout);
 status=0; 
 t = time(NULL);
 t_m = *localtime(&t);
 hh=t_m.tm_hour;
 mn=t_m.tm_min;
 fflush(stdout);
 /////////////////////////////////////////////
 //Create socket
 socket_desc = socket(AF_INET, SOCK_STREAM, 0);
 if (socket_desc == -1) {
  perror(strerror(errno));
  return(-1);
 }
 printf("Socket cree\n");
 fflush(stdout);
 t_m = *localtime(&t);
 //Preparation structure sockaddr_in
 server.sin_family = AF_INET;
 server.sin_addr.s_addr = htonl(INADDR_ANY);// INADDR_ANY;
 server.sin_port = htons(PORT );
 if( bind(socket_desc, (struct sockaddr *)&server, sizeof(server)) < 0){
  //print the error message
  perror(strerror(errno));
  fflush(stdout); 
  return(-1);
 }
 /////////////////////////////////////////////////////////
 while ( 0 == 0 ) { 
  // On attend la connection du client :
  /////////////////////////////////////////////////////////
  breakreadwrite=false;
  listen(socket_desc, 3);
  printf("Waiting for incoming connections...\n");
  fflush(stdout);
  c = sizeof(struct sockaddr_in);
  client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);
  if (client_sock < 0){
   perror(strerror(errno));
   return 1;
  }
  t = time(NULL);
  t_m = *localtime(&t);
  hh = t_m.tm_hour;
  mn = t_m.tm_min;
  printf("Client connected %dh %dmn (%d/%d/%d) \n", hh, mn, 
	 t_m.tm_mday, t_m.tm_mon+1, t_m.tm_year+1900);
  fflush(stdout);
  sprintf(buffer, "$CONNECTIONOK!");
  status=write(client_sock, buffer, strlen(buffer)); 
  /*  bzero(buffer, 256); 
  read_size = recv(client_sock, buffer, 256, 0);
  if (read_size > 0 ){
   printf("Message recu = %s \n", buffer)	;
   fflush(stdout);
   }*/
  /////////////////////////////////////////////////////////
  // Le client est connecte, on gere les messages de correction dans la boucle qui suit
  // lancement du thread de lecture sur le socket 
  td[0].thread_id = 0;
  pthread_create(&threads[0], NULL, readsocket, (void *)&td[0]);
  while (( 1 != 0 ) && (breakreadwrite == false)) {
    /////////////////////////////////////////////////////////
    //
    if (status < 0) {
     breakreadwrite=true;
    }
   } // fin de while ( 1 != 0 ) && (breakreadwrite == false)	  
   sleep(10);
  } // fin de while ( 0 == 0 ) 
 return(0);
}
