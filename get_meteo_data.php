<?php
include "config.php";
// include "date_nuit.php";
//////////////////////////////
//////////////////////////////////////////////////
function getDataCurvemeteo($CurveName)
{ 
global $db;
global $nb_obs;
global $jour;
global $lastValue_meteo;

//
global $datedeb;
global $datefin;



 ////////////////////////////
 // dates debut et fin de nuit :
// $jour="2018-04-26";
 $datedeb="";
 $datefin="";
 getDates(); // ->  $datedeb  et $datefin au format "yyyy-mm-jj hh:mn:ss"
 $sql = "SELECT ". $CurveName.",dateTime FROM `archive` where dateTime >= '". strtotime($datedeb)."' and dateTime <= '".strtotime($datefin)."' order by dateTime" ;

 $result = "";
 $cpt = 0;
 $value="null";
 //echo "<br>";
 //echo $sql." <br>  ".$datedeb."    ->    ".$datefin." </br>";
 $req = mysqli_query($db,$sql) or die('Erreur SQL !<br>'.$sql.'<br>'.mysqli_error());

 while  ($data = mysqli_fetch_assoc($req)) {
  $lastvalue=$value;
  $value = $data[$CurveName];
  $lastValue_meteo = $value;
  if ( $value == "" ) {
   $value=$lastvalue;
   }
  else {
   if ( $CurveName == "windSpeed" )  $value=$value/3.6;
  }
  $timestamp = $data['dateTime']*1000; // date UTC
  $y_t=$data['dateTime'];  
  $year = intval(date("Y", $y_t));
  $month = intval(date("m", $y_t));
  $day = intval(date("d", $y_t));
  $hour = intval(date("H", $y_t));
  $minute = intval(date("i", $y_t));
  $second = intval(date("s", $y_t));
  $datetime = sprintf("%d,%d,%d,%d,%d,%d GMT",$year,$month,$day,$hour,$minute,$second);
   
  if ($cpt == 0) {
   $result = $result." [".((strtotime($datedeb))*1000).", null],";
   $result = $result. " [".$timestamp.",".$value."]";
   $lastValue_meteo = $value;
  }
  else {
   $result = $result. ", [".$timestamp.",".$value."]";
  }
  $cpt++;
  
 }
 if ($cpt  > 0) {
   $result = $result." ,[".((strtotime($datefin))*1000).", null]";
 }
 if ($cpt == 0) {
  $result = " [".((strtotime($datetime0))*1000).", null],"; 
  $result = $result." [".((strtotime($datefin))*1000).", null]";
 }
 return $result;
}



function getFinalCurvemeteo($tab_name_meteo)
{
 // protect
 global $lastValue_meteo;
 global $graph_windmax;
 global $graph_humiditymax;
 global $dewpoint_max;
 global $humidity_max;
 global $windmax;
 global $db;

 for($i = 0; $i < count($tab_name_meteo); ++$i) {
  $CurveName = $tab_name_meteo[$i];
  $lastValue_meteo_precedente=$lastValue_meteo;
  $result  = getDataCurvemeteo($CurveName);
  $tab_title_meteo[] = $CurveName;
  $tab_donnes_meteo[] = $result;
  $tab_lastValue_meteo[] = sprintf("%0.1f",$lastValue_meteo);
 }
 $curves_meteo = "series:[";
 for($i = 0; $i < count($tab_title_meteo); ++$i) {
  $last_value_meteo = $tab_lastValue_meteo[$i];
  $CurveName = $tab_name_meteo[$i];
  if ($CurveName == "windSpeed" ) {
       $tab_title_meteo[$i]="Wind";
    $last_value_meteo = $last_value_meteo ."m/s";
    }
  if ($CurveName == "outHumidity" )  {
       $tab_title_meteo[$i]="Humidity ";
    $last_value_meteo = $last_value_meteo ." %";
   } 
   if ($CurveName == "Dewpoint" )  {
       $tab_title_meteo[$i]="Dewpoint";
    $last_value_meteo = $last_value_meteo ." °";
   }
  if ($CurveName == "outTemp" )  {
       $tab_title_meteo[$i]="Temperature";
    $last_value_meteo = $last_value_meteo ." (°C)";
   }
  
   if ($i == (count($tab_name_meteo)-1) ) {
     if ($CurveName == "Dewpoint") {
      $curves_meteo = $curves_meteo. sprintf("{name:'%s',data:[%s]}",$tab_title_meteo[$i]  ,$tab_donnes_meteo[$i]);
     }
     else {
      $curves_meteo = $curves_meteo. sprintf("{showInLegend:false, name:'%s',data:[%s]},",$tab_title_meteo[$i]  ,$tab_donnes_meteo[$i]);
     }
   }
   else {
    $curves_meteo = $curves_meteo. sprintf("{name:'%s',data:[%s]},",$tab_title_meteo[$i] ,$tab_donnes_meteo[$i]);
   }
 }
/////////////////////////////////
 if ($CurveName == "windSpeed"){ 
   $lv_d =intval($windmax*10) ; 
   $lastV = $lv_d / 10 ;
   $lastValue_meteo=$lastValue_meteo/3.6;
   $lv_dc =intval($lastValue_meteo*10) ; 
   $lastVc = $lv_dc / 10 ;
   if ( $lastValue_meteo > $windmax ) {
     $curves_meteo = $curves_meteo .' '. $graph_windmax . "], labels: {
		 items:[{html:'Wind speed ". $lastVc . "m/s > ". $lastV . "m/s',style: {left:'55%',top:'1px',color:'#990000'}}]}";
   }
   else {
     $curves_meteo = $curves_meteo .' '. $graph_windmax . "], labels: {
		 items:[{html:'Wind speed threshold = ". $lastV . "m/s',style: {left:'55%',top:'1px',color:'#55EEEE'}}]}";
   }
 }
 else if ($CurveName == "outHumidity") { 
   $lv_dc =intval($lastValue_meteo) ; 
   $lastVc = $lv_dc ;
   if (!$db) { 
      $curves_meteo = $curves_meteo .' '. $graph_humiditymax . "], labels: {
	 items:[{html:'No access to METEO host',style:{left:'50%',top:'60px',color:'yellow'}},{html:'Humidity threshold= ". $humidity_max . "%',style:{left:'50%',top:'2px',color:'#55EEEE'}}]}";
   } else {
   if ( $lastValue_meteo > $humidity_max ) {
    $curves_meteo = $curves_meteo .' '. $graph_humiditymax . "], labels: {
		 items:[{html:'Humidity threshold: ". $lastVc."% > " . $humidity_max . "%',style: {left:'50%',top:'2px',color:'#990000'}}]}";
   }
   else {
     $curves_meteo = $curves_meteo = $curves_meteo .' '. $graph_humiditymax . "], labels: {
	 items:[{html:'Humidity threshold= ". $humidity_max . "%',style:{left:'50%',top:'2px',color:'#55EEEE'}}]}";
    
   }
  }
  }
 else if ($CurveName == "Dewpoint") { 
   $diff=($lastValue_meteo_precedente-$lastValue_meteo);
   $lv_inter=intval($lastValue_meteo_precedente*10);
   $lv_p=$lv_inter/10; // temperature
   $lv_inter=intval($lastValue_meteo*10);
   $lv_d=$lv_inter/10; // dewpoint
   $lastV=intval($diff*10);
   $diff=$lastV / 10;
   if (!$db) { $diff=$dewpoint_max;}
   if ( $diff < $dewpoint_max ){
     $curves_meteo = $curves_meteo . "], labels: { items:[{html:'Dewpoint depression threshold : ". $diff."°C( ". $lv_p."- ".$lv_d." <" . $dewpoint_max . ")',style: {left:'0%',top:'5px',color:'#990000'}}]}";
   }
   else {
     $curves_meteo = $curves_meteo . "], labels: { items:[{html:'Dewpoint depression threshold = ". $dewpoint_max . 
   "°C ',style: {left:'30%',top:'5px',color:'#55EEEE'}}]}";
   }
  }
 else {
  $curves_meteo = $curves_meteo . "]";
 }
 

 return $curves_meteo;
}



function getLastData($CurveName)
{

global $db;
global $nb_obs;
global $jour;
global $lastValue_meteo;
//
global $datedeb;
global $datefin;
 ////////////////////////////
 // dates debut et fin de nuit :
 //$jour="2018-04-26";
 $datedeb="";
 $datefin="";
 getDates(); // ->  $datedeb  et $datefin au format "yyyy-mm-jj hh:mn:ss"

 $sql = "SELECT ". $CurveName." FROM `archive` where dateTime = '".strtotime($datefin)."' and ". $CurveName." is not NULL" ;
 $value="null";
 $req = mysqli_query($db,$sql) or die('Erreur SQL !<br>'.$sql.'<br>'.mysqli_error());
 while  ($data = mysqli_fetch_assoc($req)) {
  $value = $data[$CurveName];
 if ( $CurveName == "windSpeed" )  $value=$value/3.6;
 }
 return $value;
}


$db = mysqli_connect($host_meteo, $login_meteo, $pass_meteo,'weewx');
if ($db) { 
//echo'<p> get_meteo_data </p>';
 $tab_name_meteo =  array ("outHumidity");
 $graph22_data= getFinalCurvemeteo($tab_name_meteo);
 $tab_name_meteo =  array ("windSpeed");
 $graph222_data= getFinalCurvemeteo($tab_name_meteo);
 $tab_name_meteo =  array ("outTemp","Dewpoint");
 $graph2222_data= getFinalCurvemeteo($tab_name_meteo);
 $tab_name_meteo =  array ("outTemp");
 $graph3_data= getFinalCurvemeteo($tab_name_meteo);


 $tempo=getLastData("outTemp");
 $pos=strpos($tempo,".");
 $notGraph_temp = substr($tempo,0,$pos+2);
 
 $tempo=getLastData("windSpeed");
 $pos=strpos($tempo,".");
 $notGraph_wind = substr($tempo,0,$pos+2);

 $tempo=getLastData("outHumidity");
 $pos=strpos($tempo,".");
 $notGraph_humidity = substr($tempo,0,$pos+2);
 mysqli_close($db);
}
?>
