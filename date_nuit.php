<?php
function getDates()
{global $datedeb;// format "yyyy-mm-jj hh:mn:ss"
 global $datefin;// format "yyyy-mm-jj hh:mn:ss"
 global $jour;// format "yyyy-mm-jj"
 global $now_hour; //=date('G'); // hour (0 to 23)
//////////////////////////
include "ephemerides.php"; // tables lever/coucher du soleil  
//date_default_timezone_set("Europe/Paris"); fait dans php.ini
//$jour="2018-04-26";
if (isset($jour)){
  $numjour=date('z',strtotime($jour)); // day of the year (from 0 through 365)
  // On cherche les heures de lever et coucher du soleil qui dependent du jour dans l'annee:
  $heurelever=$hlever[$numjour]+1; //1H avant
  $heurecoucher=$hcoucher[$numjour];//1H apres 
  $datedeb = date( 'Y-m-d ',strtotime($jour));
  $datedeb = $datedeb.' '. $heurecoucher .':00:00';
  $datefin = date( 'Y-m-d ',(strtotime($jour)+3600*24));
  $datefin = $datefin.' '. $heurelever .':00:00';
 } else {
  $numjour=date('z'); // day of the year (from 0 through 365)
  // heure locale
  //$now_hour=date('G'); // hour (0 to 23)
  //////////////////////////////////////
  // On cherche les heures de lever et coucher du soleil qui dependent du jour dans l'annee:
  $heurelever=$hlever[$numjour]+1; //1H avant
  $heurecoucher=$hcoucher[$numjour];//1H apres
  // On observe entre $heurelever et $heurecoucher (heures locales)
  if ($now_hour < $heurecoucher) { // acq terminee pour le jour en cours ou 2nde partie de la nuit
   //echo " acq terminee pour le jour en cours ou 2nde partie de la nuit"."<br>";
   //
   $dd = date('Y-m-d',time()-3600*24);
   $datedeb=$dd.' '.$heurecoucher.':00:00';
   //
   $dd = date('Y-m-d',time());
   $datefin = $dd.' '.$heurelever.':00:00';;
   //
  } else { // affiche 1ere partie de la nuit
   $dd = date('Y-m-d',time());
   $datedeb=$dd.' '.$heurecoucher.':00:00';
   //
   $dd = date('Y-m-d',time()+3600*24*1);
   $datefin = $dd.' '.$heurelever.':00:00';;
  //
  }
 }
}
//$datedeb="";
//$datefin="";
//$jour=$yy.'-'.$mm.'-'.$jj;
//$jour="2018-02-03";
//getDates();
//echo $datedeb."<br>";
//echo $datefin."<br>";
?>
