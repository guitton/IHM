var timer = null;
var OldDiv = "";
var newFrame = null;
var TimerRunning = false;
// ## PARAMETRE D'AFFICHAGE du CALENDRIER ## //
//si enLigne est a true , le calendrier s'affiche sur une seule ligne,
//sinon il prend la taille specifie par defaut;
  
var largeur = "210";
var separateur = "/";
  
/* ##################### CONFIGURATION ##################### */
  
/* ##- INITIALISATION DES VARIABLES -##*/
var calendrierSortie = '';
//Date actuelle
var today = '';
//Mois actuel
var current_month = '';
//Annee actuelle
var current_year = '' ;
//Jours actuel
var current_day = '';
//Nombres de jours depuis le debut de la semaine
var current_day_since_start_week = '';
//On initialise le nom des mois et le nom des jours en VF
var month_name = new Array('Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre');
var day_name = new Array('L','M','M','J','V','S','D');
//permet de recuperer l'input sur lequel on a clicke et de le remplir avec la date formatee
var myObjectClick = null;
//Classe qui sera detecte pour afficher le calendrier
var classMove = "calendrier";
//Variable permettant de savoir si on doit garder en memoire le champs input clicke
var lastInput = null;
//Div du calendrier
var div_calendar = "";
var year, month, day = "";

/* ##################### FIN DE LA CONFIGURATION ##################### */
function datobs(yyyymmjj)
{
var myArray = [
"20180426", "20180425", "20180424", "20180421", "20180420", "20180419", "20180418", "20180417", "20180416", "20180406", "20180405", "20180404", "20180401", "20180329", "20180328", "20180327", "20180326", "20180325", "20180323", "20180322", "20180321", "20180320", "20180319", "20180315", "20180313", "20180124", "20180123", "20171218", "20171217", "20171216", "20171212", "20171206", "20171205", "20171204", "20171130", "20171129", "20171128", "20171127", "20171123", "20171117", "20171116", "20171115", "20171111", "20171110", "20171109", "20171102", "20171101", "20171031", "20171030", "20171028", "20171027", "20171026", "20171025", "20171024", "20171023", "20171022", "20171021", "20171020", "20171019", "20171018", "20171017", "20171016", "20171012", "20171011", "20171010", "20171009", "20171008", "20171007", "20171006", "20171005", "20170929", "20170928", "20170927", "20170921", "20170920", "20170918", "20170917", "20170914", "20170913", "20170912", "20170907", "20170906", "20170905", "20170904", "20170903", "20170902", "20170901", "20170830", "20170829", "20170828", "20170827", "20170826", "20170825", "20170824", "20170823", "20170822", "20170821", "20170820", "20170819", "20170818", "20170817", "20170816", "20170809", "20170808", "20170807", "20170806", "20170803", "20170802", "20170801", "20170731", "20170730", "20170727", "20170726", "20170725", "20170724", "20170718", "20170717", "20170712", "20170711", "20170710", "20170705", "20170704", "20170703", "20170701", "20170629", "20170628", "20170627", "20170626", "20170614", "20170613", "20170608", "20170528", "20170527", "20170517", "20170516", "20170515", "20170510", "20170509", "20170504", "20170503", "20170406", "20170330", "20170329", "20170316", "20170315", "20170314", "20170313", "20170308", "20170302", "20170222", "20170221", "20170220", "20170216", "20170215", "20170206", "20170124", "20170119", "20170118", "20170117", "20170116", "20170109", "20170104", "20170103", "20161221", "20161213", "20161212", "20161211", "20161208", "20161207", "20161201", "20161130", "20161129", "20161117", "20161116", "20161115", "20161114", "20161108", "20161107", "20161102", "20161101", "20161031", "20161027", "20161005", "20161004", "20160929", "20160928", "20160927", "20160920", "20160919", "20160913", "20160908", "20160907", "20160906", "20160905", "20160901", "20160831", "20160828", "20160825", "20160822", "20160818", "20160817", "20160815", "20160814", "20160811", "20160810", "20160809", "20160808", "20160807", "20160805", "20160803", "20160802", "20160801", "20160728", "20160727", "20160726", "20160725", "20160721", "20160720", "20160719", "20160712", "20160711", "20160710", "20160707", "20160704", "20160701", "20160630", "20160629", "20160628", "20160627", "20160624", "20160623", "20160621", "20160617", "20160616", "20160614", "20160609", "20160608", "20160607", "20160602", "20160531", "20160527", "20160526", "20160524", "20160523", "20160519", "20160518", "20160517", "20160504", "20160503", "20160502", "20160427", "20160426", "20160425", "20160420", "20160419", "20160418", "20160414", "20160411", "20160407", "20160406", "20160325", "20160324", "20160323", "20160322", "20160321", "20160315", "20160314", "20160310", "20160307", "20160229", "20160223", "20160218", "20160216", "20160215", "20160204", "20160203", "20160202", "20160201", "20160128", "20160127", "20160125", "20160113", "20160110", "20160109", "20160108", "20160107", "20160106", "20160105", "20160104", "20160103", "20160102", "20160101", ""];
return myArray.indexOf(yyyymmjj) ;
}
/* ################################ */
function FormatNumberLength(num, length) {
    var r = "" + num;
    while (r.length < length) {
        r = "0" + r;
    }
    return r;
}
/* ########################## Fonction de convesion d'une date en jour julien ########################## */
function gregorian_to_jd(year, month, day)
{
var GREGORIAN_EPOCH = 2457754.5+1;

alert(GREGORIAN_EPOCH - 1) +
           (365 * (year - 1)) +
           Math.floor((year - 1) / 4) +
           (-Math.floor((year - 1) / 100)) +
           Math.floor((year - 1) / 400) +
           Math.floor((((367 * month) - 362) / 12) +
           ((month <= 2) ? 0 :
                               (leap_gregorian(year) ? -1 : -2)
           ) +
           day);
}

//########################## Fonction permettant de remplacer "document.getElementById"  ########################## //
function $(element){
    return document.getElementById(element);
}
  
  
//Permet de faire glisser une div de la gauche vers la droite
function slideUp(bigMenu,smallMenu){
    //Si le timer n'est pas finit on detruit l'ancienne div
    if(parseInt($(bigMenu).style.left) < 0){
        $(bigMenu).style.left = parseInt($(bigMenu).style.left) + 10 + "px";
        $(smallMenu).style.left  =parseInt($(smallMenu).style.left) + 10 + "px";
        timer = setTimeout('slideUp("'+bigMenu+'","'+smallMenu+'")',10);
    }
    else{
        clearTimeout(timer);
        TimerRunning = false;
        $(smallMenu).parentNode.removeChild($(smallMenu));
        //alert("timer up bien kill");
    }
}
  
//Permet de faire glisser une div de la droite vers la gauche
function slideDown(bigMenu,smallMenu){
    if(parseInt($(bigMenu).style.left) > 0){
        $(bigMenu).style.left = parseInt($(bigMenu).style.left) - 10 + "px";
        $(smallMenu).style.left =parseInt($(smallMenu).style.left) - 10 + "px";
        timer = setTimeout('slideDown("'+bigMenu+'","'+smallMenu+'")',10);
    }
    else{
        clearTimeout(timer);
        TimerRunning = false;      
        //delete de l'ancienne
        $(smallMenu).parentNode.removeChild($(smallMenu));
        //alert("timer down bien kill");
    }
}
  
//Creation d'une nouvelle div contenant les jours du calendrier
function CreateDivTempo(From){
    if(!TimerRunning){
    var DateTemp = new Date();
    IdTemp = DateTemp.getMilliseconds();
    var  NewDiv = document.createElement('DIV');
         NewDiv.style.position = "absolute";
         NewDiv.style.top = "0px";
         NewDiv.style.width = "100%";
         NewDiv.className = "ListeDate";
         NewDiv.id = IdTemp;
         //remplissage
         NewDiv.innerHTML = CreateDayCalandar(year, month, day);
          
    $("Contenant_Calendar").appendChild(NewDiv);
     
        if(From == "left"){
            TimerRunning = true;
            NewDiv.style.left = "-"+largeur+"px";
            slideUp(NewDiv.id,OldDiv);
        }
        else if(From == "right"){
            TimerRunning = true;
            NewDiv.style.left = largeur+"px";
            slideDown(NewDiv.id,OldDiv);
        }
        else{
            "";
            NewDiv.style.left = 0+"px";
        }
        $('Contenant_Calendar').style.height = NewDiv.offsetHeight+"px";
        $('Contenant_Calendar').style.zIndex = "200";
        OldDiv = NewDiv.id;
    }
}
  
//########################## FIN DES FONCTION LISTENER ########################## //
/*Ajout du listener pour detecter le click sur l'element et afficher le calendrier
uniquement sur les textbox de class css date */
  
//Fonction permettant d'initialiser les listeners
function init_evenement(){
    //On commence par affecter une fonction Ã  chaque evenement de la souris
    if(window.attachEvent){
        document.onmousedown = start;
        document.onmouseup = drop;
    }
    else{
        document.addEventListener("mousedown",start, false);
        document.addEventListener("mouseup",drop, false);
    }
}
//Fonction permettant de recuperer l'objet sur lequel on a clicke, et l'on recupere sa classe
function start(e){
    //On initialise l'evenement s'il n'a aps ete cree ( sous ie )
    if(!e){
        e = window.event;
    }
    //Detection de l'element sur lequel on a clicke
    var monElement = null;
    monElement = (e.target)? e.target:e.srcElement;
    if(monElement != null && monElement)
    {
        //On appel la fonction permettant de recuperer la classe de l'objet et assigner les variables
        getClassDrag(monElement);
         
        if(myObjectClick){
            initialiserCalendrier(monElement);
            lastInput = myObjectClick;
        }
    }
}
function drop(){
         myObjectClick = null;
}
//########################## Fonction permettant de recuperer la liste des classes d'un objet ##########################//
function getClassDrag(myObject){
    with(myObject){
        var x = className;
        listeClass = x.split(" ");
        //On parcours le tableau pour voir si l'objet est de type calendrier
        for(var i = 0 ; i < listeClass.length ; i++){
            if(listeClass[i] == classMove){
                myObjectClick = myObject;
                break;
            }
        }
    }
}
  
//########################## Pour combler un bug d'ie 6 on masque les select ########################## //
function masquerSelect(){
        var ua = navigator.userAgent.toLowerCase();
        var versionNav = parseFloat( ua.substring( ua.indexOf('msie ') + 5 ) );
        var isIE        = ( (ua.indexOf('msie') != -1) && (ua.indexOf('opera') == -1) && (ua.indexOf('webtv') == -1) );
  
        if(isIE && (versionNav < 7)){
             svn=document.getElementsByTagName("SELECT");
             for (a=0;a<svn.length;a++){
                svn[a].style.visibility="hidden";
             }
        }
}
  
function montrerSelect(){
       var ua = navigator.userAgent.toLowerCase();
        var versionNav = parseFloat( ua.substring( ua.indexOf('msie ') + 5 ) );
        var isIE        = ( (ua.indexOf('msie') != -1) && (ua.indexOf('opera') == -1) && (ua.indexOf('webtv') == -1) );
        if(isIE && versionNav < 7){
             svn=document.getElementsByTagName("SELECT");
             for (a=0;a<svn.length;a++){
                svn[a].style.visibility="visible";
             }
         }
}
  
function createFrame(){
    newFrame = document.createElement('iframe');
    newFrame.style.width = largeur+"px";
    newFrame.style.height = div_calendar.offsetHeight-10+"px";
    newFrame.style.zIndex = "0";
    newFrame.frameBorder="0";
    newFrame.style.position = "absolute";
    newFrame.style.display = "block";
    //newFrame.style.opacity = 0 ;
    //newFrame.filters.alpha.opacity = 0 ;
    newFrame.style.top = 0 +"px";
    newFrame.style.left = 0+"px";
    div_calendar.appendChild(newFrame);
}
  
//######################## FONCTIONS PROPRE AU CALENDRIER ########################## //
//Fonction permettant de passer a l'annee precedente
function annee_precedente(){
  
    //On recupere l'annee actuelle puis on verifie que l'on est pas en l'an 1 :-)
    if(current_year == 1){
        current_year = current_year;
    }
    else{
        current_year = current_year - 1 ;
    }
    //et on appelle la fonction de generation de calendrier
    CreateDivTempo('left');
    //calendrier(   current_year , current_month, current_day);
}
  
//Fonction permettant de passer a  l'annee suivante
function annee_suivante(){
    //Pas de limite pour l'ajout d'annee
    current_year = current_year +1 ;
    //et on appelle la fonction de generation de calendrier
    //calendrier(   current_year , current_month, current_day);
    CreateDivTempo('right');
}
  
//Fonction permettant de passer au mois precedent
function mois_precedent(){
  
    //On recupere le mois actuel puis on verifie que l'on est pas en janvier sinon on enleve une annee
    if(current_month == 0){
        current_month = 11;
        current_year = current_year - 1;
    }
    else{
        current_month = current_month - 1 ;
    }
    //et on appel la fonction de generation de calendrier
    CreateDivTempo('left');
    //calendrier(   current_year , current_month, current_day);
}
  
//Fonction permettant de passer au mois suivant
function mois_suivant(){
    //On recupere le mois actuel puis on verifie que l'on est pas en janvier sinon on ajoute une annee
    if(current_month == 11){
        current_month = 0;
        current_year = current_year  + 1;
    }
    else{
        current_month = current_month + 1;
    }
    //et on appel la fonction de generation de calendrier
    //calendrier(   current_year , current_month, current_day);
    CreateDivTempo('right');
}
  
//Fonction principale qui genere le calendrier
//Elle prend en parametre, l'annee , le mois , et le jour
//Si l'annee et le mois ne sont pas renseignes , la date courante est affecte par defaut
function calendrier(year, month, day){
    //Aujourd'hui si month et year ne sont pas renseignes
    if(month == null || year == null){
        today = new Date();
    }
    else{
        //month = month - 1;
        //Creation d'une date en fonction de celle passee en parametre
        today = new Date(year, month , day);
    }
  
    //Mois actuel
    current_month = today.getMonth()
     
    //Annee actuelle
    current_year = today.getFullYear();
     
    //Jours actuel
    current_day = today.getDate();
     
    //######################## ENTETE ########################//
    //Ligne permettant de changer l'annee et de mois
    var month_bef = "<a href=\"javascript:mois_precedent()\" style=\"position:absolute;left:30px;z-index:200;\" > < </a>";
    var month_next = "<a href=\"javascript:mois_suivant()\" style=\"position:absolute;right:30px;z-index:200;\"> > </a>";
    var year_next = "<a href=\"javascript:annee_suivante()\" style=\"position:absolute;right:5px;z-index:200;\" >&nbsp;&nbsp; > > </a>";
    var year_bef = "<a href=\"javascript:annee_precedente()\" style=\"position:absolute;left:5px;z-index:200;\"  > < < &nbsp;&nbsp;</a>";
    calendrierSortie = "<p class=\"titleMonth\" style=\"position:relative;z-index:200;\"> <a href=\"javascript:alimenterChamps('')\" style=\"float:left;margin-left:3px;color:#cccccc;font-size:10px;z-index:200;\"> Effacer la date </a><a href=\"javascript:masquerCalendrier()\" style=\"float:right;margin-right:3px;color:red;font-weight:bold;font-size:12px;z-index:200;\">X</a>&nbsp;</p>";
    //On affiche le mois et l'annee en titre
    calendrierSortie += "<p class=\"titleMonth\" style=\"float:left;position:relative;z-index:200;\">" + year_next + year_bef+  month_bef + "<span id=\"curentDateString\">" + month_name[current_month]+ " "+ current_year +"</span>"+ month_next+"</p><div id=\"Contenant_Calendar\">";
    //######################## FIN ENTETE ########################//
     
    //Si aucun calendrier n'a encore ete cree :
    if(!document.getElementById("calendrier")){
        //On cree une div dynamiquement, en absolute, positionne sous le champs input
        div_calendar = document.createElement("div");
         
        //On lui attribut un id
        div_calendar.setAttribute("id","calendrier");
         
        //On definit les proprietes de cette div ( id et classe )
        div_calendar.className = "calendar";
         
        //Pour ajouter la div dans le document
        var mybody = document.getElementsByTagName("body")[0];
         
        //Pour finir on ajoute la div dans le document
        mybody.appendChild(div_calendar);
    }
    else{
            div_calendar = document.getElementById("calendrier");
    }
     
    //On inserer dans la div, le contenu du calendrier genere
    //On assigne la taille du calendrier de faÃ§on dynamique ( on ajoute 10 px pour combler un bug sous ie )
    var width_calendar = largeur+"px";
    //Ajout des elements dans le calendrier
    calendrierSortie = calendrierSortie + "</div><div class=\"separator\"></div>";
    div_calendar.innerHTML = calendrierSortie;
    div_calendar.style.width = width_calendar;
    //On remplit le calendrier avec les jours
//  alert(CreateDayCalandar(year, month, day));
    CreateDivTempo('');
}
  
function CreateDayCalandar(){
     
    // On recupere le premier jour de la semaine du mois
    var dateTemp = new Date(current_year, current_month,1);
     
    //test pour verifier quel jour etait le premier du mois
    current_day_since_start_week = (( dateTemp.getDay()== 0 ) ? 6 : dateTemp.getDay() - 1);
     
    //variable permettant de verifier si l'on est deja rentre dans la condition pour eviter une boucle infinie
    var verifJour = false;
     
    //On initialise le nombre de jour par mois
    var nbJoursfevrier = (current_year % 4) == 0 ? 29 : 28;
    //Initialisation du tableau indiquant le nombre de jours par mois
    var day_number = new Array(31,nbJoursfevrier,31,30,31,30,31,31,30,31,30,31);
     
    var x = 0
     
    //On initialise la ligne qui comportera tous les noms des jours depuis le debut du mois
    var list_day = '';
    var day_calendar = '';
    //On remplit le calendrier avec le nombre de jour, en remplissant les premiers jours par des champs vides
    for(var nbjours = 0 ; nbjours < (day_number[current_month] + current_day_since_start_week) ; nbjours++){
         
        // On boucle tous les 7 jours pour creer la ligne qui comportera le nom des jours en fonction des<br />
        // parametres d'affichage
        if(verifJour == false){
            for(x = 0 ; x < 7 ; x++){
                if(x == 6){
                    list_day += "<span>" + day_name[x] + "</span>";
                }
                else{
                    list_day += "<span>" + day_name[x] + "</span>";
                }
            }
            verifJour = true;
        }
        //et enfin on ajoute les dates au calendrier
        //Pour gerer les jours "vide" et eviter de faire une boucle on verifie que le nombre
	// de jours correspond bien au nombre de jour du mois
        if(nbjours < day_number[current_month]){
	   // varymd=new Date(current_year, current_month, nbjours+1)    
	   s_varymd = FormatNumberLength(current_year,4);
	   s_varymd += FormatNumberLength(current_month+1,2)+FormatNumberLength((nbjours+1),2);
	   //alert(s_varymd+" "+datobs(s_varymd));
	   if  ( datobs(s_varymd) < 0 ) { 
	    day_calendar += "<span class=\"DayDateMissing\" onclick=\"alimenterChamps(this.innerHTML)\">" + (nbjours+1) + "</span>";
	   }
           else {
            day_calendar += "<span class=\"DayDateBold DayDate \" onclick=\"alimenterChamps(this.innerHTML)\">" + (nbjours+1) + "</span>";
           }
        }
    }
  
    //On ajoute les jours "vide" du debut du mois
    for(i  = 0 ; i < current_day_since_start_week ; i ++){
        day_calendar = "<span>&nbsp;</span>" + day_calendar;
    }
    //On met egalement a jour le mois et l'annee
    $('curentDateString').innerHTML = month_name[current_month]+ " "+ current_year;
    return (list_day  + day_calendar);
}
  
function initialiserCalendrier(objetClick){
        //on affecte la variable definissant sur quel input on a clicke
        myObjectClick = objetClick;
         
        if(myObjectClick.disabled != true){
            //on verifie que le champs n'est pas deja remplit, sinon on va se positionner sur la date du champs
            if(myObjectClick.value != ''){
                //On utilise la chaine de separateur
                    var reg=new RegExp("/", "g");
                    var dateDuChamps = myObjectClick.value;
                    var tableau=dateDuChamps.split(reg);
                    calendrier( tableau[2] , tableau[1] - 1 , tableau[0]);
            }
            else{
                //on creer le calendrier
                calendrier(objetClick);
                 
  
            }
            //puis on le positionne par rapport a l'objet sur lequel on a clicke
            //positionCalendar(objetClick);
            positionCalendar(objetClick);
            fadePic();
            //masquerSelect();
            createFrame();
        }
  
}
  
 //Fonction permettant de trouver la position de l'element ( input ) pour pouvoir positionner le calendrier
function ds_getleft(el) {
    var tmp = el.offsetLeft;
    el = el.offsetParent
    while(el) {
        tmp += el.offsetLeft;
        el = el.offsetParent;
    }
    return tmp;
}
  
function ds_gettop(el) {
    var tmp = el.offsetTop;
    el = el.offsetParent
    while(el) {
        tmp += el.offsetTop;
        el = el.offsetParent;
    }
    return tmp;
}
  
//fonction permettant de positionner le calendrier
function positionCalendar(objetParent){
    //document.getElementById('calendrier').style.left = ds_getleft(objetParent) + "px";
    document.getElementById('calendrier').style.left = ds_getleft(objetParent) + "px";
    //document.getElementById('calendrier').style.top = ds_gettop(objetParent) + 20 + "px" ;
    document.getElementById('calendrier').style.top = ds_gettop(objetParent) + 20 + "px" ;
    // et on le rend visible
    document.getElementById('calendrier').style.visibility = "visible";
}
//Fonction permettant d'alimenter le champs
function alimenterChamps(daySelect){
        if(daySelect != ''){
            lastInput.value= formatInfZero(daySelect) + separateur + formatInfZero((current_month+1)) + separateur +current_year;
        }
        else{
            lastInput.value = '';
        }
        masquerCalendrier();
}
function masquerCalendrier(){
        fadePic();
        //On Masque la frame /!\
        newFrame.style.display = "none";
        //document.getElementById('calendrier').style.visibility = "hidden";
        //montrerSelect();
}
  
function formatInfZero(numberFormat){
        if(parseInt(numberFormat) < 10){
                numberFormat = "0"+numberFormat;
        }
         
        return numberFormat;
}
  
function CreateSpan(){
    var spanTemp = document.createElement("span");
        spanTemp.className = "";
        spanTemp.innerText = "";
        spanTemp.onClick = "";
    return spanTemp;
}
  
//######################## FONCTION PERMETTANT DE VERIFIER UNE DATE SAISI PAR L'UTILISATEUR ########################//
function CheckDate(d) {
      // Format de la date : JJ/MM/AAAA .
      var j=(d.substring(0,2));
      var m=(d.substring(3,5));
      var a=(d.substring(6));
      var regA = new RegExp("[0-9]{4}");
      alert(regA.test(a));
      if ( ((isNaN(j))||(j<1)||(j>31))) {
         return false;
      }
  
      if ( ((isNaN(m))||(m<1)||(m>12))) {
         return false;
      }
  
      if ((isNaN(a))||(regA.test(a))) {
         return false;
      }
      return true;
}
//######################## FONCTION PERMETTANT D'AFFICHER LE CALENDRIER DE FACON PROGRESSIVE ########################//
var max = 100;
var min = 0;
var opacite=min;
up=true;
var IsIE=!!document.all;
  
  
function fadePic(){
try{       
                var ThePic=document.getElementById("calendrier");
                if (opacite < max && up){opacite+=5;}
                if (opacite>min && !up){opacite-=5;}
                IsIE?ThePic.filters[0].opacity=opacite:document.getElementById("calendrier").style.opacity=opacite/100;
                 
                if(opacite<max && up){
                    timer = setTimeout('fadePic()',10);
                }
                else if(opacite>min && !up){
                    timer = setTimeout('fadePic()',10);
                }
                else{
                    if (opacite==max){up=false;}
                    if (opacite<=min){up=true;}
                    clearTimeout(timer);
                }
}
catch(error){
    alert(error.message);
}
}
  
window.onload = init_evenement;


