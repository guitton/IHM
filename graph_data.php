<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

<!--
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/themes/gray.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
-->
<script src="../Highcharts/code/highcharts.js"></script>
<script src="../Highcharts/code/modules/exporting.js"></script>
<script src="../Highcharts/code/themes/gray.js"></script>
<script src="../Highcharts/code/highcharts-more.js"></script>

<script type="text/javascript"> 
<?php
  global $jour;
  global $now_hour; 
  // date_default_timezone_set("Europe/Paris"); fait dans php.ini
  $now_hour=date('G');
  //$jour="2018-04-26";
  include "date_nuit.php";
  include "get_meteo_status.php"; 
  include "get_dome_status.php"; 
  include "get_seuils.php";
  include "get_meteo_data.php";
  include "get_allsky_data.php"; 
  include "get_seeing_data.php";
 ?>

$(function () {
 // container pour le graphique meteo
 $('#graph_humidity').highcharts({
        chart: {
            type: 'spline',
   zoomType: 'x'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
  exporting: {
         enabled: false
  },
        xAxis: {
    type: 'datetime',
    dateTimeLabelFormats: { // don't display the dummy year
                year: '%Y',
    month: '%b \'%y',
                day: '%e. %b',
       hour: '%H:%M',
    minute: '%H:%M',
    second: '%H:%M:%S',
    millisecond: '%H:%M:%S'            },
            title: {
                text: ''
            },
   tickInterval: 2*3600*1000.,
   //min: Date_UTC(2016,2,21,18,10,0,0),
           // max: Date_UTC(2016,2,23,18,10,0,0),
   
        },
        yAxis: {

            title: {
                text: 'Humidity (%)'
            },
   type: 'linear',
            minorTickInterval: 1,
   min: 0,     
   max: 100
        },
        tooltip: {
            headerFormat: '',
            pointFormat: '{point.x:%e. %b %H:%M:%S}   humidity:{point.y:.1f} %'
        },

        plotOptions: {
            spline: {
                marker: {
                    enabled: false
                }
            }
        },
 
 <?php echo $graph22_data ?>
 
 });
 // container pour le graphique meteo
 $('#graph_windspeed').highcharts({
        chart: {
            type: 'spline',
   zoomType: 'x'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
  exporting: {
         enabled: false
  },
        xAxis: {
    type: 'datetime',
    dateTimeLabelFormats: { // don't display the dummy year
                year: '%Y',
    month: '%b \'%y',
                day: '%e. %b',
       hour: '%H:%M',
    minute: '%H:%M',
    second: '%H:%M:%S',
    millisecond: '%H:%M:%S'            },
            title: {
                text: 'TU time'
            },
   tickInterval: 2*3600*1000.,
        },
        yAxis: {

            title: {
                text: 'Wind (m/s)'
            },
   type: 'linear',
   ceiling: 25,
   min: 0
        },
        tooltip: {
            headerFormat: '',
            pointFormat: '{point.x:%e. %b %H:%M:%S}   windSpeed:{point.y:.1f} m/s'
        },

        plotOptions: {
            spline: {
                marker: {
                    enabled: false
                }
            }
        },
 
 <?php echo $graph222_data ?>
 
 });
 
  // container pour le graphique meteo
 $('#graph_temp').highcharts({
        chart: {
            type: 'spline',
   zoomType: 'x'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
  exporting: {
         enabled: false
  },
        xAxis: {
    type: 'datetime',
    dateTimeLabelFormats: { // don't display the dummy year
                year: '%Y',
    month: '%b \'%y',
                day: '%e. %b',
       hour: '%H:%M',
    minute: '%H:%M',
    second: '%H:%M:%S',
    millisecond: '%H:%M:%S'            },
            title: {
                text: ''
            },
   tickInterval: 2*3600*1000.,
        },
        yAxis: {
            title: {
                text: 'Temperature (°C)'
            },
   type: 'linear',
            minorTickInterval: 0.5,
   min: -5,     
   max: 40
        },
        tooltip: {
            pointFormat: '{series.name}:{point.y:.1f}°C<br> ',
   valueSuffix: '°C',
   shared: true
        },
        plotOptions: {
            spline: {
                marker: {
                    enabled: false
                }
            }
        },
     
 <?php echo $graph2222_data ?>
 
 });
 // container pour le graphique cloudy
 $('#graph_cloudy').highcharts({
        chart: {
            type: 'spline',
   
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
  exporting: {
         enabled: false
  },
        xAxis: {
           type: 'datetime',
           dateTimeLabelFormats: { 
                year: '%Y',
                month: '%b \'%y',
                day: '%e. %b',
                hour: '%H:%M',
                minute: '%H:%M',
                second: '%H:%M:%S',
                millisecond: '%H:%M:%S'            
            },
            title: {
                text: 'TU time'
            },
            tickInterval: 2*3600*1000.,  
        },
        yAxis: {
            title: {
                text: 'Cloud Cover (%)'
            },
            type: 'linear',
            minorTickInterval: 0.5,
            min: 0,     
            max: 100
        },
        tooltip: {
            headerFormat: '',
            pointFormat: '{point.x:%e. %b %H:%M:%S}   Cloudy:{point.y:.1f} %'
        },
        plotOptions: {
            spline: {
                marker: {
                    enabled: false
                }
            }
        },
  <?php echo $graph1_data ?>
 });
 
 // container pour le graphique des R0
 $('#graph_r0').highcharts({
        chart: {
            type: 'scatter',
   zoomType: 'x'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
  exporting: {
         enabled: false
  },
        xAxis: {
    type: 'datetime',
    dateTimeLabelFormats: { // don't display the dummy year
                year: '%Y',
    month: '%b \'%y',
                day: '%e. %b',
       hour: '%H:%M',
    minute: '%H:%M',
    second: '%H:%M:%S',
    millisecond: '%H:%M:%S'            },
            title: {
                text: ''
            },
   tickInterval: 2*3600*1000.,
        },
        yAxis: {

            title: {
                text: "Seeing ('')"
            },
   type: 'linear',
            minorTickInterval: 1,
   min: 0,
   max: 3
        },
        tooltip: {
            headerFormat: '',
            pointFormat: "{point.x:%e. %b %H:%M:%S}  eps:{point.y:.2f} ''"
        },

      

        plotOptions: {
            series:{ marker:{radius:2}}
        },
 
 <?php echo $graph3_data ?>
 
 });
  
 // container pour le graphique des isoplanetism
 $('#graph_isop').highcharts({
        chart: {
            type: 'scatter',
   zoomType: 'x'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
  exporting: {
         enabled: false
  },
        xAxis: {
    type: 'datetime',
    dateTimeLabelFormats: { // don't display the dummy year
                year: '%Y',
    month: '%b \'%y',
                day: '%e. %b',
       hour: '%H:%M',
    minute: '%H:%M',
    second: '%H:%M:%S',
    millisecond: '%H:%M:%S'            },
            title: {
                text: 'TU time'
            },
   tickInterval: 2*3600*1000.,
        },
        yAxis: {

            title: {
                text: "isoplanetism ('')"
            },
   type: 'linear',
            minorTickInterval: 1,
   min: 0,
   max: 4
        },
        tooltip: {
            headerFormat: '',
            pointFormat: "{point.x:%e. %b %H:%M:%S}   isoplanetism:{point.y:.2f} ''"
        },

        

        plotOptions: {
            series:{ marker:{radius:2}}
        },
 
 <?php echo $graph333_data ?>
 
 });
 // container pour le graphique scintillation
 $('#graph_ind').highcharts({
        chart: {
            type: 'scatter',
                    radius : 1,
   zoomType: 'x'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
  exporting: {
         enabled: false
  },
        xAxis: {
    type: 'datetime',
    dateTimeLabelFormats: { // don't display the dummy year
                year: '%Y',
    month: '%b \'%y',
                day: '%e. %b',
       hour: '%H:%M',
    minute: '%H:%M',
    second: '%H:%M:%S',
    millisecond: '%H:%M:%S'            },
            title: {
                text: ''
            },
   tickInterval: 2*3600*1000.,
        },
        yAxis: {
            title: {
                text: 'Scintillation (%)'
            },
   type: 'linear',
   min: 0,
   max: 20
        },
        tooltip: {
            headerFormat: '',
            pointFormat: '{point.x:%e. %b %H:%M:%S}   scintillation:{point.y:.1f} %'
        },

        plotOptions: {
            series:{ marker:{radius:2}}
        },
 
 <?php echo $graph33_data ?>

 });

}); 
</script>
 





