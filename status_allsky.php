<?php
/**
* @class cats_Allsky
* @details connect by tcp/ip on AllSky software and display
* status in webpage
* Note: You must have extension=php_sockets.dll actived in php.ini (version wamp)
* usage:
  exemple for index.php
	include "status_allsky.php";
	$allsky = new cats_allSky("192.134.16.150",5555,true);
	ou
	$allsky = new cats_allSky($host_tcp,$port_tcp,true);
	les variables $host_tcp, $port_tcp ainsi que les parametres de connexion mysql sont
	dans le fichier  config.php
	
	si true = connexion tcp/ip
	si false = connexion mysql
	
	Tips: For refresh page avery 10 s add <meta http-equiv="refresh" content="10" > 
*
*
*/
include "config.php";
# permet d'ignorer le message de mysql_connect is deprecated la nouvelle biblio est mysqli
error_reporting(E_ALL ^ E_DEPRECATED);
class cats_allSky
{
	private $host_tcp = "192.134.16.150";  //192.134.16.150
	private $port_tcp = "5555";
	private $status = "CRITICAL";

	// Connexion et s�lection de la base

    function __construct($host,$port,$is_socket) {
	
		global $host_tcp;
		global $port_tcp;
		$this->host    = $host_tcp;
		$this->port    = $port_tcp;
		$this->status = "CRITICAL";
		global $mode;
		$mode = $is_socket;

		
		// test si fonctionnement en socket ou sur la database
		if ($mode)
		{
			$date = date("d-m-Y");
			$heure = date("H:i");			
			$status = $this->send_msg("get_status");
			$cloudy = $this->send_msg("get_cloudy");
		} else
		{
			$reply = $this->get_status_from_db();
			$date= $reply[0];
			$heure= $reply[1];
			$status = $reply[2];
			$cloudy = $reply[3];			
		}

		$this->Display_in_tab($date,$heure,$status,$cloudy);	
    }
	


			/**
	* @brief send message to allsky software
	* @return reply message
	*/
	function disp_error($message)
	{
			// display button
		$date = date("d-m-Y");
		$heure = date("H:i");
		echo" <table border='0' cellspacing='5' BGCOLOR='#0000FF'>
		   <!-- Ici, on �crira le contenu du tableau -->
		   <tr ALIGN=CENTER>
				<td colspan='3'> <FONT COLOR='white'><b>ALLSKY</b> </td>
		   </tr>
		   <tr> 
				
				<td bordercolor='#000000' bgcolor='#FF0000'>".$date." at ".$heure." <i>UT</i></td>
				<td bordercolor='#000000' bgcolor='#FF0000'> E R R O R</td>
				<td bordercolor='#000000' bgcolor='#FF0000'> ". $message . "</td>
		   </tr>
		</table>";
	}

	/**
	* @brief read status from database
	* @return reply message
	*/
	function get_status_from_db()
	{
		$status = 0;
		$date = "21-10-2015";
		$heure = "14:36";
		$message = "NA";
		$value = "0";
		global $host;
		global $login;
		global $pass;
		// get status from DB
		// Connexion et s�lection de la base
		$db=mysqli_connect($host, $login, $pass,'cats');
		
		$sql = "SELECT * FROM `cats_status` WHERE `id_instrument` = 2" ;
		$req = mysqli_query($db,$sql);
		if ( ! $req  ) { // or die('Erreur SQL !<br>'.$sql.'<br>'.mysqli_error());
		    $date = date("d/m/Y");
			$heure = date("H:i:s");
			$message= "Could not connect by sql ALLSKY";
			$value="ERROR";
		}
        else {		
		 // get last cloudy
		
		while($data = mysqli_fetch_assoc($req))
		{ 
			$MysqlTimestamp = strtotime($data['timestamp']);
			$message = $data['message'];
			$value = $data['value'];
			$status = $data['status_instr'];

			// $status = 4    --> mode LIVE
			if ($value == -999 & $status != 4 )
			{
				$value = "NA";
			} else
			{
				$value = 100-$value;
				$value = round($value, 2);
			}
			$date = date("d/m/Y", $MysqlTimestamp);
			$heure = date("H:i:s", $MysqlTimestamp);
		}
		$localTimestamp = time() - date('Z');
		// marge d'erreur entre les horloges
		if ( $localTimestamp  > ($MysqlTimestamp+200) || $localTimestamp  < ($MysqlTimestamp-200) ) 
		{
			//$this->disp_error("ERROR : Could not connect to ALLSKY.");
			//exit;
			 $date = date("d/m/Y", $MysqlTimestamp);
			$heure = date("H:i:s", $MysqlTimestamp);
			$message= "ERROR time ALLSKY";
			$value="ERROR";
		}
	    }
		$reponse = array($date,$heure,$message,$value);
		return $reponse;
	}

	
	/**
	* @brief send message to allsky software
	* @param[in] message ie. get_status
	* @return reply message
	*/
	function send_msg($message)
	{
		// create socket
		$socket = socket_create(AF_INET, SOCK_STREAM, 0) or die("Could not create socket\n");
		// connect to server
		echo '<!--';
		$result="ERROR : Could not connect to ALLSKY";
		$res = socket_connect($socket, $this->host, $this->port); // or die("ERROR : Could not connect to ALLSKY\n");
		echo '-->';
		if (!$result)
		{
			//$this->disp_error("ERROR : Could not connect to ALLSKY");
			//exit;
		}
		else {
		// send string to server
		$res = socket_write($socket,$message, strlen($message));
		if (!$res)
		{
		//	$this->disp_error("ERROR : Could not send data to ALLSKY");
		//	exit;
		}
		else {
		// get server response
		$res = socket_read ($socket, 2048,PHP_NORMAL_READ);
		$res=false;
		if (!$res)
		{
			//$this->disp_error("ERROR : Could not read server response from ALLSKY");
			//exit;
		}
		else {
		$result=$res;
		}
		// close socket
		socket_close($socket);
		}
		}
		return $result;
	}

	/**
	* @brief display information
	* @details display information in table
	*/
	function Display_in_tab($date,$heure,$status,$cloudy)
	{ 
		// display button
		global $mode;
		$title = "ALLSKY";
		$bleu="'#00FFFF'";
		$vert="'#00FF00'";
		$jaune="'#FFFF00'";
		$orange="'orange'";
		$rouge="'red'";
		
		$statuscolor= $vert;
		if ( substr($cloudy,0,3) == substr("ERROR",0,3 ) ) {
		 $statuscolor= $rouge;
		}
		else {
		 if ( substr($cloudy,0,1) == substr("NA",0,1 ) )
	  	   { $statuscolor= $jaune;
		    $cloudy="Cloudy ".$cloudy;
			}
		else {
		 if ( $cloudy > 80 ) {
		   $statuscolor= $orange;}
		 else {
		   $statuscolor= $jaune;}
		 $cloudy="Cloudy:".$cloudy."%";
		}
		}
		echo" <table border='0' cellspacing='2' BGCOLOR='#0000FF'>
		   <!-- Ici, on �crira le contenu du tableau -->
		   <tr ALIGN=CENTER>
				<td colspan='3'> <FONT COLOR='white'><b>" . $title . "</b> </td>
		   </tr>
		   <tr> 
				
				<td bordercolor='#000000' bgcolor='#00FFFF' ><font size='2'>".$date." at ".$heure." <i>UT</font></i></td>
				<td bordercolor='#000000' bgcolor= ".$statuscolor."><font size='2'>"
				.$status.
				 "</font></td>
				<td bordercolor='#000000' bgcolor=".$statuscolor."><font size='2'>   " . $cloudy . "</font></td>
		   </tr>
		</table>";
	}
	

}

?>
