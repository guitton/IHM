<?php
include "config.php";
$last_mesure = 300;
$lastValue = 0;
$lastDateAcq = "";
$sql_tag = "";
#error_reporting(E_ALL);
error_reporting(E_ALL ^ E_DEPRECATED);
// Connexion et s�lection de la base
$db = mysqli_connect($host, $login, $pass,'cats');

function getDataCurve($CurveName,$module)
{
	global $last_mesure ;
	global $lastValue ;
	global $lastDateAcq ;
     global $db;
    // On affiche toutes les donnees cloudy de la derniere nuit ou de la nuit en cours
	//
	//
	 $now_hour=date('G');
	// Calcul des offsets TU / heute locale
	$d=new DateTime("now");
	$tz=new DateTimeZone('Europe/Paris');
	$d->setTimezone($tz);
	$offset=$d->getOffset();
	//echo $offset."<br>";
	$offsethour=$offset/3600;
	//echo $offsethour ."<br>";
	// Les donnees allsky sont en heures locale: on doit les afficher en heure locale
	// datation du systeme en heures locale
	// On observe entre 19h et 7h heure locale
	// les donnees ALLSKY sont dat�es en heure locale
     if ($now_hour < 19-$offsethour) { // acq terminee pour le jour en cours ou 2nde partie de la nuit
        //echo " acq terminee pour le jour en cours"."<br>";
		$datedeb = date('Y-m-d 19:0:0',time()-3600*24);
		$datefin = date('Y-m-d 7:0:0',time());
		$yeardeb = intval(date('Y',time()-3600*24));
		$moisdeb = intval(date('m',time()-3600*24))-1;
		$jourdeb = intval(date('d',time()-3600*24));
		 $yd = intval(date("Y",time()-3600*24)); 
	     $md = intval(date("m",time()-3600*24))-1;
	     $dd = intval(date("d",time()-3600*24));
		 $yf = intval(date("Y",time())); 
	     $mf = intval(date("m",time()))-1;
	     $df = intval(date("d",time()));	
	 } 
	 else { // 1ere partie de la nuit
		  //echo "1ere partie de la nuit "."<br>";
		  $datedeb = date('Y-m-d 19:0:0',time());  
		  $datefin = date('Y-m-d 7:0:0',time()+3600*24);
		  $yeardeb = intval(date('Y'));
		  $moisdeb = intval(date('m'))-1;
		  $jourdeb = intval(date('d'));	
		  $yd = intval(date("Y",time())); 
	      $md = intval(date("m",time()))-1;
	      $dd = intval(date("d",time()));
		  $yf = intval(date("Y",time()+3600*24)); 
	      $mf = intval(date("m",time()+3600*24))-1;
	      $df = intval(date("d",time()+3600*24));	  
	}
	$nb=0; // nb > 0 pour test jours precedents
  	if ($nb > 0 ) {
 	 $datedeb =date('Y-m-d 19:0:0',time()-3600*24*$nb);
	 $yd = intval(date("Y",time()-3600*24*$nb)); 
	 $md = intval(date("m",time()-3600*24*$nb))-1;
	 $dd = intval(date("d",time()-3600*24*$nb));
 	 $datefin =date('Y-m-d 7:0:0',time()-3600*24*$nb+3600*24);
	 $yf = intval(date("Y",time()-3600*24*$nb+3600*24)); 
	 $mf = intval(date("m",time()-3600*24*$nb+3600*24))-1;
	 $df = intval(date("d",time()-3600*24*$nb+3600*24));	
	}
	$sql = "SELECT * FROM `cats_allsky` where timestamp >= '". $datedeb . "' and timestamp <= '". $datefin .  "'"  ;
    $result = "";
	$req = mysqli_query($db,$sql) or die('Erreur SQL !<br>'.$sql.'<br>'.mysqli_error());
	$cpt = 0;
	while($data = mysqli_fetch_assoc($req))
	{	
		$value = $data['value'];
		//$timestamp = (strtotime($data['timestamp'])+$offset)*1000;
		$timestamp = (strtotime($data['timestamp']))*1000;
		$y_ts=$data['timestamp'];
		//$y_t=strtotime($y_ts)+$offset;
		$y_t=strtotime($y_ts);
		$year = intval(date("Y", $y_t ));
		$month = intval(date("m", $y_t))-1;
		$day = intval(date("d", $y_t));
		$hour = intval(date("H", $y_t));
		$minute = intval(date("i", $y_t));
		$second = intval(date("s", $y_t));
	   
		$datetime = sprintf("%d,%d,%d,%d,%d,%d",$year,$month,$day,$hour,$minute,$second);
	 
		if ($cpt == 0)
		{$value0= "null";
		 $datetime0= sprintf("%d, %d, %d,19,0,0",$yd,$md,$dd);
	     $result = $result. " [Date.UTC(".$datetime0."),".$value0."],";
		 $lastValue = $value;
		 $lastDateAcq = $hour."h".$minute."\'";
		}
		$result = $result. " [".$timestamp.",".$value."],";
		$cpt++;
		
	}
	if ($cpt  > 0) {
		 $value= "null";
		 $datetime0= sprintf("%d, %d, %d, 7,0,0",$yf,$mf,$df);
		 $result = $result. " [Date.UTC(".$datetime0."),".$value."]";
		}
	// dans le cas ou on n'a pas de donn�es
	if ($cpt == 0) {
	 $value= "null";
		 $datetime0= sprintf("%d, %d, %d, 19,0,0",$yd,$md,$dd);
		 $result = $result. " [Date.UTC(".$datetime0."),".$value."],";
		 $datetime0= sprintf("%d, %d, %d, 7,0,0",$yf,$mf,$df);
		 $result = $result. " [Date.UTC(".$datetime0."),".$value."],";
		 
			$lastValue = $value;
			$lastDateAcq = "19h0\'";
		}

	//echo "cpt= ".$cpt. "<br>"  ;
    return $result;
}

function getLastStatus()
{global $db;
	$msg = "IDLE";
	$cpt = 0;
	$date = "1970-01-01 00:00:00";
	$sql = "SELECT * FROM `cats_allsky` WHERE 1 ORDER BY timestamp DESC LIMIT 1";
	$req = mysqli_query($sql) or die('Erreur SQL !<br>'.$sql.'<br>'.mysqli_error());
	$num_rows = mysqli_num_rows($req);
	while($row  = mysqli_fetch_assoc($req))
	{
		$value = $row ['value'];
		$timestamp = $row ['timestamp'];
	}
	$msq = $timestamp . "  > " . $value ;
	return $msg;
}

function getFinalCurve($tab_name,$tab_module)
{
	// protect
	global $lastValue;
	global $lastDateAcq;
	unset($tab_title);
	unset($tab_donnee);
	unset($tab_lastValue);
	if (count($tab_name) != count($tab_module))
	return "Error";
	for($i = 0; $i < count($tab_name); ++$i) {
		$CurveName = $tab_name[$i];
		$module = $tab_module[$i]; 
		$result  = getDataCurve($CurveName,$module);
		$tab_title[] = $CurveName;
		$tab_donnee[] = $result;
		$tab_module[] = $module;
		$tab_lastValue[] = sprintf("%0.1f",$lastValue);
	}
	
	$curves = "series:[";
	for($i = 0; $i < count($tab_title); ++$i) {
		$last_value = $tab_lastValue[$i];
		if ($last_value == -999)
		{
			$last_value= "no current record";

			} else
			{
			 $last_value = $last_value ." %";
			}
		//$curves = $curves. sprintf("{name:'%s',data:[%s]},",$tab_title[$i] . " "  . ": " . $last_value . " @ ". $lastDateAcq . " (local)" ,$tab_donnee[$i]);
		$curves = $curves. sprintf("{showInLegend:false,  name:'%s',data:[%s]},",$tab_title[$i] ,$tab_donnee[$i]);
	}
	$curves = $curves . "]";

	return $curves;
}




// DATA FOR GRAPHIC 
$tab_name =  array ("Cloudy");
$tab_module =  array ("1");
$graph1_data= getFinalCurve($tab_name,$tab_module);
//echo $graph1_data;
$graph1_title = "Cloudy";

mysqli_close($db); 

?>
